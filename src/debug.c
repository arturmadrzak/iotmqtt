#include <stdio.h>
#include "mqtt.h"
#include "debug.h"

log_level_t current_log_level = LOG_DEBUG;

const char *level_desc[] = {
    "ERROR",
    "WARNING",
    "INFO",
    "DEBUG"
};

const char *type_desc[] = {
    "RESERVED_BEGIN",
    "CONNECT",
    "CONNACK",
    "PUBLISH",
    "PUBACK",
    "PUBREC",
    "PUBREL",
    "PUBCOMP",
    "SUBSCRIBE",
    "SUBACK",
    "UNSUBSCRIBE",
    "UNSUBACK",
    "PINGREQ",
    "PINGRESP",
    "DISCONNECT",
    "RESERVED_END"
};

const char *mqtt_errno_desc[] = {
    "MQTT_ERRNO_OK",
    "MQTT_ERRNO_BROKEN_PIPE",
    "MQTT_ERRNO_TIMEOUT",
    "MQTT_ERRNO_MALFORMED_PACKET",
    "MQTT_ERRNO_BUFFER_OVF",
    "MQTT_ERRNO_WRONG_CLIENT_ID",
    "MQTT_ERRNO_USERNAME_REQUIRED",
    "MQTT_ERRNO_WRONG_QOS",
    "MQTT_ERRNO_PASS_REQUIRED",
    "MQTT_ERRNO_INVALID_VALUE",
    "MQTT_ERRNO_NOT_ENOUGH_MEMORY",
    "MQTT_ERRNO_WRONG_PACKET_ID",
};

inline void log_mqtt_frame(const char *dir, mqtt_frame_t *desc) 
{
    int i;
    log_info("%s %s, dup:%d, qos:%d, retain:%d, plen:%d",
            dir,
            type_desc[desc->header.bits.type],
            desc->header.bits.dup,
            desc->header.bits.qos,
            desc->header.bits.retain,
            desc->plen);
}

const char* mqtt_errnostr(int errno)
{
    if (errno < sizeof (mqtt_errno_desc))
        return mqtt_errno_desc[errno];
    return "Unknown";
}
