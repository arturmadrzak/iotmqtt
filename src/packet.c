#include "mqtt.h"
#include <string.h>

struct msg_connect {
    int8_t protocol_name[6];
    uint8_t protocol_level;
    union {
        uint8_t byte;
        struct {
            uint8_t reserved:1;
            uint8_t clean_session:1;
            uint8_t will_flag:1;
            uint8_t will_qos:2;
            uint8_t will_retain:1;
            uint8_t password_flag:1;
            uint8_t username_flag:1;
        } bits;
    } flags;
    uint16_t keepalive; /* big endian */
    uint8_t field_data[];
} __attribute__ ((__packed__));

struct msg_connack {
    uint8_t session_present:1;
    uint8_t reserved:7;
    uint8_t return_code;
} __attribute__ ((__packed__));

struct msg_ack {
    uint16_t packet_id;
} __attribute__ ((__packed__));

struct msg_suback {
    uint16_t packet_id;
    uint8_t rcodes[];
} __attribute__ ((__packed__));

static int field_string(const char *value, int8_t* output, int size)
{
    int len = strlen(value);
    if ((len + 2) > size)
        return -1;
    *((uint16_t*)output) = swap16(len);
    strcpy((char*)output + 2, value);
    return 2 + len;
}

static int in_field_string(uint8_t *from, int flen, char* to, int tsize)
{
    uint8_t len = swap16(*(uint16_t*)from);
    /* check string fits in to buffer and is enough bytes in input frame */
    if (len > tsize || (len + 2) > flen)
        return -1;
    strncpy(to, (char*)from + 2, len);
    return len + 2;
}

static int field_binary(uint8_t *b, uint16_t blen, uint8_t* output, int size)
{
    if ((blen + 2) > size)
        return -1;
    *((uint16_t*)output) = swap16(blen);
    memcpy(output + 2, b, blen);
    return 2 + blen;
}

static inline int packet_identifier(uint16_t value, uint8_t* output, int size)
{
    if (2 > size)
        return -1;

    *((uint16_t*)output) = swap16(value);
    return 2;
}

static inline int raw(uint8_t *b, uint32_t blen, uint8_t* output, int size)
{
    if ((int)blen > size)
        return -1;
    memcpy(output, b, blen);
    return blen;
}

static int ack_type_packet(mqtt_frame_t* desc, enum packet_type ptype)
{
    struct msg_ack *ack = (struct msg_ack*)desc->payload;
    if (desc->header.bits.type != ptype ||
            desc->plen != 2 ||
            desc->header.bits.qos != 0 ||
            desc->header.bits.dup != 0 ||
            desc->header.bits.retain != 0)
        return MQTT_ERRNO_MALFORMED_PACKET;

    return swap16(ack->packet_id);

}

int out_packet_connect(mqtt_connect_opts_t *opts, mqtt_frame_t *desc)
{
    int marker = 0, rv;
    struct msg_connect* msg = (struct msg_connect*) desc->payload;

    if (!opts->client_id)
        return MQTT_ERRNO_WRONG_CLIENT_ID;
    if (opts->password && !opts->username)
        return MQTT_ERRNO_USERNAME_REQUIRED;

    desc->plen = sizeof(struct msg_connect);
    desc->header.byte = 0;
    desc->header.bits.type = CONNECT;
    if (field_string("MQTT", msg->protocol_name, 6) <= 0)
        return MQTT_ERRNO_BUFFER_OVF;
    msg->protocol_level = CONFIG_MQTT_LEVEL;
    msg->keepalive = swap16(opts->keep_alive);
    msg->flags.bits.reserved = 0;
    msg->flags.bits.clean_session = opts->clean_session;
    msg->flags.bits.username_flag = (opts->username != NULL);
    msg->flags.bits.password_flag = (opts->password != NULL);
    msg->flags.bits.will_flag = (opts->will != NULL);
    msg->flags.bits.will_qos = (opts->will != NULL) ? opts->will->qos : 0;
    msg->flags.bits.will_retain = (opts->will != NULL) ? opts->will->retain : 0;

    if (msg->flags.bits.will_qos > QoS2)
        return MQTT_ERRNO_WRONG_QOS;
    if (msg->flags.bits.password_flag && !msg->flags.bits.username_flag)
        return MQTT_ERRNO_PASS_REQUIRED;


    if ((rv = field_string(opts->client_id, (int8_t*)&msg->field_data[marker], 23)) <= 0)
        return MQTT_ERRNO_WRONG_CLIENT_ID;
    marker += rv;
    if (msg->flags.bits.will_flag) {
        // FIXME: length must be calculated from buffer size
        if ((rv = field_string(opts->will->topic, (int8_t*)&msg->field_data[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += rv;
        if ((rv = field_binary(opts->will->msg, opts->will->mlen, &msg->field_data[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += rv;
    }

    if (msg->flags.bits.username_flag) {
                if ((rv = field_string(opts->username, (int8_t*)&msg->field_data[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += rv;

        if (msg->flags.bits.password_flag) {
            if ((rv = field_string(opts->password, (int8_t*)&msg->field_data[marker], 128)) <= 0)
                return MQTT_ERRNO_BUFFER_OVF;
            marker += rv;
        }
    }

    desc->plen += marker;
    return MQTT_ERRNO_OK;
}

int out_packet_publish(uint16_t packet_id, mqtt_msg_t *msg, mqtt_frame_t *desc)
{
    int marker = 0, rv;
    if (msg->qos > QoS2)
        return MQTT_ERRNO_WRONG_QOS;
    desc->header.byte = 0;
    desc->header.bits.type = PUBLISH;
    desc->header.bits.qos = msg->qos;
    desc->header.bits.retain = msg->retain;

    if ((rv = field_string(msg->topic,(int8_t*)&desc->payload[marker], 128)) <= 0)
        return MQTT_ERRNO_BUFFER_OVF;
    marker += rv;
    if (msg->qos > QoS0)
        marker += packet_identifier(packet_id, &desc->payload[marker], 128);
    if (msg->msg && msg->mlen) {
        if ((rv = raw(msg->msg, msg->mlen, &desc->payload[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += rv;
    }
    desc->plen = marker;
    return MQTT_ERRNO_OK;
}

static int out_packet_id(uint16_t packet_id, mqtt_frame_t *desc)
{
    int rv;
    desc->header.bits.dup = 0;
    desc->header.bits.qos = 0;
    desc->header.bits.retain = 0;
    if ((rv = packet_identifier(packet_id, desc->payload, 128)) <= 0)
        return MQTT_ERRNO_BUFFER_OVF;
    desc->plen =  rv;
    return MQTT_ERRNO_OK;
}

int out_packet_puback(uint16_t packet_id, mqtt_frame_t *desc)
{
    desc->header.bits.type = PUBACK;
    return out_packet_id(packet_id, desc);
}

int out_packet_pubrec(uint16_t packet_id, mqtt_frame_t *desc)
{
    desc->header.bits.type = PUBREC;
    return out_packet_id(packet_id, desc);
}

int out_packet_pubrel(uint16_t packet_id, mqtt_frame_t *desc)
{
    int rv = out_packet_id(packet_id, desc);
    desc->header.bits.type = PUBREL;
    desc->header.bits.qos = QoS1;
    return rv;
}

int out_packet_pubcomp(uint16_t packet_id, mqtt_frame_t *desc)
{
    desc->header.bits.type = PUBCOMP;
    return out_packet_id(packet_id, desc);
}

int out_packet_subscribe(uint16_t packet_id, mqtt_subs_opts_t *opts, uint32_t num, mqtt_frame_t *desc)
{
    int marker = 0, rv;
    uint32_t i;
    uint8_t tmp;
    if (num == 0 || opts == NULL)
        return MQTT_ERRNO_INVALID_VALUE;

    desc->header.byte = 0;
    desc->header.bits.type = SUBSCRIBE;
    desc->header.bits.qos = QoS1;

    if ((rv = packet_identifier(packet_id, desc->payload, 128)) <= 0)
        return MQTT_ERRNO_BUFFER_OVF;
    marker += rv;

    for(i = 0; i < num; i++) {
        if (!opts[i].topic)
            return MQTT_ERRNO_INVALID_VALUE;
        if ((rv = field_string(opts[i].topic, (int8_t*)&desc->payload[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += rv;
        if (opts[i].qos > QoS2)
            return MQTT_ERRNO_INVALID_VALUE;
        tmp = opts[i].qos;
        if ((rv = raw(&tmp, 1, &desc->payload[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += 1;
    }

    desc->plen = marker;
    return MQTT_ERRNO_OK;
}

int out_packet_unsubscribe(uint16_t packet_id, char *topics[], uint32_t num, mqtt_frame_t *desc)
{
    int marker = 0, rv;
    uint32_t i;

    if (num == 0 || topics == NULL)
        return MQTT_ERRNO_INVALID_VALUE;

    desc->header.byte = 0;
    desc->header.bits.type = UNSUBSCRIBE;
    desc->header.bits.qos = QoS1;

    if ((rv = packet_identifier(packet_id, desc->payload, 128)) <= 0)
        return MQTT_ERRNO_BUFFER_OVF;
    marker += rv;

    for(i = 0; i < num; i++) {
        if ((rv = field_string(topics[i], (int8_t*)&desc->payload[marker], 128)) <= 0)
            return MQTT_ERRNO_BUFFER_OVF;
        marker += rv;
    }
    desc->plen = marker;
    return MQTT_ERRNO_OK;
}

int out_packet_pingreq(mqtt_frame_t *desc)
{
    desc->header.byte = 0;
    desc->header.bits.type = PINGREQ;
    desc->plen = 0;
    return MQTT_ERRNO_OK;
}

int out_packet_disconnect(mqtt_frame_t *desc)
{
    desc->header.byte = 0;
    desc->header.bits.type = DISCONNECT;
    desc->plen = 0;
    return MQTT_ERRNO_OK;
}

int in_packet_publish(mqtt_frame_t* desc, mqtt_msg_t *msg)
{
    int32_t marker;
    uint16_t packetid;

    /* 5 is the min length of publish packet */
    if (desc->header.bits.type != PUBLISH ||desc->plen < 5)
        return MQTT_ERRNO_MALFORMED_PACKET;

    msg->qos = desc->header.bits.qos;
    msg->retain = desc->header.bits.retain;

    /* decode topic name field */
    if ((marker = in_field_string(desc->payload, desc->plen, msg->topic, 128)) <= 0)
        return MQTT_ERRNO_BUFFER_OVF;

    /* check if there is enough bytes in payload to read packetid */
    if ((uint32_t)(marker + 2) > desc->plen)
        return MQTT_ERRNO_MALFORMED_PACKET;

    if (msg->qos > QoS0) {
        /* decode packet id */
        packetid = swap16(*(uint16_t*)&desc->payload[marker]);
        marker += 2;
    }

    /* decode msg itself */
    msg->mlen = desc->plen - marker;
    memcpy(msg->msg, &desc->payload[marker], msg->mlen);

    return packetid;
}

int in_packet_connack(mqtt_frame_t* desc, mqtt_connack_opts_t *opts)
{
    struct msg_connack *connack = (struct msg_connack*)desc->payload;
    if (desc->header.bits.type != CONNACK || desc->plen != 2)
        return MQTT_ERRNO_MALFORMED_PACKET;

    if (connack->reserved != 0)
        return MQTT_ERRNO_MALFORMED_PACKET;

    opts->session_present = connack->session_present;
    opts->return_code = connack->return_code;

    return MQTT_ERRNO_OK;
}

int in_packet_pub_response(mqtt_frame_t *desc)
{
    struct msg_ack *ack = (struct msg_ack*)desc->payload;
    if (desc->plen != 2 || desc->header.bits.qos != 0 ||
        desc->header.bits.dup != 0 || desc->header.bits.retain != 0)
            return MQTT_ERRNO_MALFORMED_PACKET;

    return swap16(ack->packet_id);
}

int in_packet_puback(mqtt_frame_t* desc)
{
    return ack_type_packet(desc, PUBACK);
}

int in_packet_pubrec(mqtt_frame_t* desc)
{
    return ack_type_packet(desc, PUBREC);
}

int in_packet_pubrel(mqtt_frame_t* desc)
{
    return ack_type_packet(desc, PUBREL);
}

int in_packet_pubcomp(mqtt_frame_t* desc)
{
    return ack_type_packet(desc, PUBCOMP);
}

int in_packet_suback(mqtt_frame_t* desc, uint32_t max, suback_rc_t rcodes[], uint32_t *num)
{
    struct msg_suback* msg = (struct msg_suback*)desc->payload;
    uint16_t packetid;
    uint32_t i;
    *num = 0;
    if (desc->header.bits.type != SUBACK ||
            desc->header.bits.qos != 0 ||
            desc->header.bits.dup != 0 ||
            desc->header.bits.retain != 0)
        return MQTT_ERRNO_MALFORMED_PACKET;

    /* there is more rcodes than we can handle in rcodes array*/
    if (desc->plen > (max + 2))
        return MQTT_ERRNO_NOT_ENOUGH_MEMORY;

    /* msg has to contain: packet_id and at least one ack */
    if (desc->plen < 3)
        return MQTT_ERRNO_MALFORMED_PACKET;

    packetid = swap16(msg->packet_id);
    *num = desc->plen - 2;
    for(i = 0; i < *num; i++) {
        if (msg->rcodes[i] <= QoS2 || msg->rcodes[i] == SUBS_FAILED)
            rcodes[i] = msg->rcodes[i];
        else {
            return MQTT_ERRNO_MALFORMED_PACKET;
        }
    }
    return packetid;
}

int in_packet_unsuback(mqtt_frame_t* desc)
{
    return ack_type_packet(desc, UNSUBACK);
}

int in_packet_pingresp(mqtt_frame_t* desc)
{
    if (desc->header.bits.type != PINGRESP ||
            desc->plen != 0 ||
            desc->header.bits.qos != 0 ||
            desc->header.bits.dup != 0 ||
            desc->header.bits.retain != 0)
        return MQTT_ERRNO_MALFORMED_PACKET;

    return MQTT_ERRNO_OK;
}
