#include "mqtt.h"
#include <unistd.h>
#include <string.h>

static int task(mqtt_client_t *c, uint32_t deadline);

static int handle_remote_publication(mqtt_client_t *c, uint32_t deadline)
{
    int packet_id, rv;
    if ((rv = in_packet_publish(c->buffer.desc, &c->input_msg)) < 0)
        return rv;
    packet_id = rv;
    if ((c->buffer.desc->header.bits.qos > 0) &&
            packet_id != c->session.packet_id) {
        return MQTT_ERRNO_WRONG_PACKET_ID;
    }
    if (c->callback)
        rv = c->callback(&c->input_msg);
            
    if (c->buffer.desc->header.bits.qos == QoS1) {
        if (rv == MQTT_ERRNO_OK) {
            out_packet_puback(packet_id, c->buffer.desc);
            mqtt_frame_serialize(c->fd, c->buffer.desc, deadline);
            c->session.last_sent = mqtt_clock();
        }
    } else if (c->buffer.desc->header.bits.qos == QoS2) {
        out_packet_pubrec(packet_id, c->buffer.desc);
        mqtt_frame_serialize(c->fd, c->buffer.desc, deadline); 
        c->session.last_sent = mqtt_clock();
        mqtt_frame_deserialize(c->fd, c->buffer.desc, c->buffer.size, deadline);
        in_packet_pubrel(c->buffer.desc);
        if ((rv = c->callback(&c->input_msg)) == MQTT_ERRNO_OK) {
            out_packet_pubcomp(packet_id, c->buffer.desc);
            mqtt_frame_serialize(c->fd, c->buffer.desc, deadline); 
            c->session.last_sent = mqtt_clock();
        }
    }
    return rv;
}

static int handle_publication(mqtt_client_t *c, uint32_t deadline)
{
    int rv;
    if (c->session.pending.opublish) {
        if((rv = in_packet_pub_response(c->buffer.desc)) < 0)
            return rv;
        if (c->session.packet_id != rv)
            return MQTT_ERRNO_WRONG_PACKET_ID;
        if (c->buffer.desc->header.bits.type == PUBACK ||
                c->buffer.desc->header.bits.type == PUBCOMP) {
            c->session.pending.opublish = false;
            return MQTT_ERRNO_OK;
        } else if (c->buffer.desc->header.bits.type == PUBREC) {
            if ((rv = out_packet_pubrel(c->session.packet_id, c->buffer.desc)))
                return rv;
            if ((rv = mqtt_frame_serialize(c->fd, c->buffer.desc, deadline)))
                return rv;
        }
    }
    /* unexpected publish response packet received. Ignore it.*/
    return MQTT_ERRNO_OK;
}

static int handle_subscribe(mqtt_client_t *c)
{
    uint32_t confirmed = 0;
    uint32_t i;
    int rv;
    mqtt_subs_opts_t *opts = c->session.pending.subscribe.opts;
    uint32_t num = c->session.pending.subscribe.num;
    suback_rc_t rcodes[num]; // FIXME: it's not good idea to allocate it on stack

    c->session.pending.subscribe.opts = NULL;
    c->session.pending.subscribe.num = 0;
    if ((rv = in_packet_suback(c->buffer.desc, num, rcodes, &confirmed)) < 0)
        return rv;
    if (rv != c->session.packet_id)
        return MQTT_ERRNO_WRONG_PACKET_ID;
    if (confirmed < num)
        return MQTT_ERRNO_TOO_FEW_ACKS;
    for(i = 0; i < num; i++) {
        if (rcodes[i] != opts[i].qos) {
            if (rcodes[i] == SUBS_FAILED)
                return MQTT_ERRNO_SUBSCRIBE_FAILED;
            return MQTT_ERRNO_SUBS_WRONG_QOS;
        }
    }
    return MQTT_ERRNO_OK;
}

static int handle_ping(mqtt_client_t *c)
{
    int rv;
    if ((rv = in_packet_pingresp(c->buffer.desc)))
        return rv;
    if (c->session.pending.ping) {
        c->session.pending.ping = false;
        c->session.last_sent = mqtt_clock();
    }
    return MQTT_ERRNO_OK;
}

static int send_keep_alive(mqtt_client_t *c, uint32_t deadline)
{
    int rv;
    out_packet_pingreq(c->buffer.desc);
    if (!(rv = mqtt_frame_serialize(c->fd, c->buffer.desc, deadline)))
        c->session.pending.ping = true;
    return rv;
}

static int task(mqtt_client_t *c, uint32_t deadline)
{
    int rv;
    uint32_t time_from_last_send = mqtt_clock() - c->session.last_sent;
    //printf("time_from_last_send: %u\n", time_from_last_send);
    if (time_from_last_send >= c->session.keep_alive) {
        send_keep_alive(c, deadline);
    }
    if((rv = mqtt_frame_deserialize(c->fd, c->buffer.desc, c->buffer.size, deadline)))
        return rv;
    
    switch(c->buffer.desc->header.bits.type) {
        case PUBLISH:
        case PUBREL:
            rv = handle_remote_publication(c, deadline);
            break;
        case SUBACK:
            rv = handle_subscribe(c);
            break;
        case PUBACK:
        case PUBREC:
        case PUBCOMP:
            rv = handle_publication(c, deadline);
            break;
        case PINGRESP:
            rv = handle_ping(c);
            break;
        default:
            rv = MQTT_ERRNO_WRONG_PACKET_TYPE;   
    }
    return rv;
}

int mqtt_connect(mqtt_client_t *c, mqtt_connect_opts_t *opts, bool *session_present)
{
    int rv;
    uint32_t deadline = c->timeout ? mqtt_clock() + c->timeout : 0;
    mqtt_connack_opts_t ack;
    memset(&ack, 0, sizeof(ack));

    if ((rv = out_packet_connect(opts, c->buffer.desc)))
        return rv;
    if ((rv = mqtt_frame_serialize(c->fd, c->buffer.desc, deadline)))
        return rv;
    if ((rv = mqtt_frame_deserialize(c->fd, c->buffer.desc, c->buffer.size, deadline)))
        return rv;
    if ((rv = in_packet_connack(c->buffer.desc, &ack)))
        return rv;
    if (session_present) 
        *session_present = ack.session_present;
    c->session.keep_alive = opts->keep_alive;
    c->session.packet_id = 1;
    c->session.last_sent = mqtt_clock();
    return ack.return_code;
}

int mqtt_disconnect(mqtt_client_t *c)
{
    uint32_t deadline = c->timeout ? mqtt_clock() + c->timeout : 0;
    out_packet_disconnect(c->buffer.desc);
    return mqtt_frame_serialize(c->fd, c->buffer.desc, deadline);
}

int mqtt_publish(mqtt_client_t *c, mqtt_msg_t *msg)
{
    int rv;
    uint32_t deadline = c->timeout ? (mqtt_clock() + c->timeout) : 0;
    
    c->session.packet_id++;
    /* send publish packet with message */
    if ((rv = out_packet_publish(c->session.packet_id, msg, c->buffer.desc)))
        return rv;
    if ((rv = mqtt_frame_serialize(c->fd, c->buffer.desc, deadline)))
        return rv;
    
    if (msg->qos > QoS0) {
        c->session.pending.opublish = true;
        do {
            rv = task(c, deadline);
        } while(c->session.pending.opublish && rv == MQTT_ERRNO_OK);
        if (rv) {
            c->session.pending.opublish = false;
            return rv;
        }
    }
    c->session.last_sent = mqtt_clock();
    return MQTT_ERRNO_OK;
}

int mqtt_subscribe(mqtt_client_t *c, mqtt_subs_opts_t *opts, uint32_t num)
{
    int rv;
    uint32_t deadline = c->timeout ? mqtt_clock() + c->timeout : 0;

    c->session.packet_id++;
    if ((rv = out_packet_subscribe(c->session.packet_id, opts, num, c->buffer.desc)))
        return rv;
    if ((rv = mqtt_frame_serialize(c->fd, c->buffer.desc, deadline)))
        return rv;
    c->session.pending.subscribe.opts = opts;
    c->session.pending.subscribe.num = num;
    do {
        rv = task(c, deadline);
    } while(c->session.pending.subscribe.opts && rv == MQTT_ERRNO_OK);
    if (rv) {
        c->session.pending.subscribe.opts = NULL;
        c->session.pending.subscribe.num = 0;
        return rv;
    }

    c->session.last_sent = mqtt_clock();
    return MQTT_ERRNO_OK;
}

int mqtt_loop(mqtt_client_t *c)
{
    uint32_t deadline = c->timeout ? mqtt_clock() + c->timeout : 0;
    return task(c, deadline);
}

