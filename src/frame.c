#include "mqtt.h"
#include "debug.h"
#include <string.h>

static int calc_remaining_len(uint32_t len, uint8_t *bytes)
{
    uint8_t *b = bytes;
    if (len > 268435455) return -1;
    do {
        *b = len % 0x80;
        len >>= 7;
        if (len) *b |= 0x80;
        b++;
    } while(len);
    return (int)(b - bytes);
}

/*
 * returns:
 * -1 - error
 *  0 - no error
 */
static int parse_remaining_len(uint8_t bytes[4], int len, uint32_t *value)
{
    uint8_t *b = bytes;
    uint32_t chunk;
    int i;
    if (len > 4) 
        return -1;
    *value = 0;
    for(i = 0; i < len; i++, b++) {
        chunk = (*b) & ~0x80;
        chunk <<= (i*7);
        (*value) |= chunk;
    }
    return 0;
}

static int e_read(mqtt_io_t *fd, uint8_t *b, uint32_t len, uint32_t deadline)
{
    int rv;
    uint32_t actual;
    if (deadline) {
        actual = mqtt_clock();
        if(actual >= deadline)
            return MQTT_ERRNO_TIMEOUT;
    }
    if ((rv = mqtt_io_read(fd, b, len, deadline ? (deadline - actual) : 0)) == 0)
        return MQTT_ERRNO_INVALID_VALUE;
    return rv;
}

static int e_write(mqtt_io_t *fd, uint8_t *b, uint32_t len, uint32_t deadline)
{
    int rv;
    uint32_t actual;
    if (deadline) {
        actual = mqtt_clock();
        if(actual >= deadline)
            return MQTT_ERRNO_TIMEOUT;
    }
    if ((rv = mqtt_io_write(fd, b, len, deadline ? (deadline - actual) : 0)) == 0)
        return MQTT_ERRNO_INVALID_VALUE;

    return rv;
}

int mqtt_frame_serialize(mqtt_io_t *fd, mqtt_frame_t *desc, uint32_t deadline)
{
    int rv, remlen;
    uint8_t tmp[4];
    
    if (desc->header.bits.type == 0 || desc->header.bits.type == 15)
        return MQTT_ERRNO_WRONG_PACKET_TYPE;
    
    if (desc->header.bits.qos > QoS2)
        return MQTT_ERRNO_MALFORMED_PACKET;
    
    if ((remlen = calc_remaining_len(desc->plen, tmp)) <= 0)
        return MQTT_ERRNO_MALFORMED_PACKET;
    
    if ((rv = e_write(fd, &desc->header.byte, 1, deadline)) < 0)
        return rv;
    
    if (e_write(fd, tmp, remlen, deadline) < 0)
        return MQTT_ERRNO_BROKEN_PIPE;

    if (desc->plen) {
        if (e_write(fd, desc->payload, desc->plen, deadline) < 0)
            return MQTT_ERRNO_BROKEN_PIPE;
    }
    log_mqtt_frame(">", desc);
    return MQTT_ERRNO_OK;
}

int mqtt_frame_deserialize(mqtt_io_t *fd, mqtt_frame_t *desc, uint32_t size, uint32_t deadline)
{
    uint8_t tmp[4];
    int i, rv;
    memset(tmp, 0, sizeof(tmp)); 
    if (size < sizeof(mqtt_frame_t))
        return MQTT_ERRNO_INVALID_VALUE;
    /* timeout error is reported only if no first byte is received.
     * In other case some bytes can be lost, so comunication is not reliable anymore */
    if ((rv = e_read(fd, &desc->header.byte, 1, deadline)) < 0)
        return rv;

    if (desc->header.bits.type <= RESERVED_BEGIN || desc->header.bits.type >= RESERVED_END)
        return MQTT_ERRNO_WRONG_PACKET_TYPE;
    if (desc->header.bits.qos > QoS2)
        return MQTT_ERRNO_WRONG_QOS;
    
    for (i = 0; i < 4; i++) {
        if (e_read(fd, &tmp[i], 1, deadline) < 0)
            return MQTT_ERRNO_BROKEN_PIPE;
        if (!(tmp[i] & 0x80)) break;
    }
    if (parse_remaining_len(tmp, i + 1, &desc->plen)) {
        return MQTT_ERRNO_MALFORMED_PACKET;
    }
    if (desc->plen > (size - sizeof(mqtt_frame_t)))
        return MQTT_ERRNO_BUFFER_OVF;

    if (desc->plen > 0) {
        i = desc->plen;
        if (e_read(fd, desc->payload, i, deadline) < 0)
            return MQTT_ERRNO_BROKEN_PIPE;
    }

    log_mqtt_frame("<", desc);
    return MQTT_ERRNO_OK;
}

/* vim: set ts=4 sw=4 sts=4 et */

