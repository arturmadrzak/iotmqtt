#ifndef _MQTT_H
#define _MQTT_H

#include <stdbool.h>
#include "system.h"
#include "packet.h"

enum {
    MQTT_ERRNO_OK,
    MQTT_ERRNO_BROKEN_PIPE = -127,
    MQTT_ERRNO_TIMEOUT,
    MQTT_ERRNO_WRONG_CLIENT_ID,
    MQTT_ERRNO_WRONG_QOS,
    MQTT_ERRNO_WRONG_PACKET_ID,
    MQTT_ERRNO_WRONG_PACKET_TYPE,
    MQTT_ERRNO_MALFORMED_PACKET,
    MQTT_ERRNO_BUFFER_OVF,
    MQTT_ERRNO_USERNAME_REQUIRED,
    MQTT_ERRNO_PASS_REQUIRED, /* -120 */
    MQTT_ERRNO_INVALID_VALUE,
    MQTT_ERRNO_NOT_ENOUGH_MEMORY,
    MQTT_ERRNO_TOO_FEW_ACKS,
    MQTT_ERRNO_SUBSCRIBE_FAILED, /* -115 */
    MQTT_ERRNO_SUBS_WRONG_QOS,
} mqtt_errno;

static inline uint16_t swap16(uint16_t x)
{
    return (x >> 8) | (x << 8);
}

/* must return MQTT_ERRNO_OK */
typedef int (*mqtt_msg_cb)(mqtt_msg_t *msg);

typedef struct {
    mqtt_io_t fd;
    uint32_t timeout;
    mqtt_msg_t input_msg;
    mqtt_msg_cb callback;

    struct {
        uint32_t size;
        mqtt_frame_t *desc;
    } buffer;

    struct {
        uint32_t keep_alive; /* */
        uint16_t packet_id;
        uint32_t last_sent; /* time when last packet was sent */
        struct {
            uint8_t opublish:1;
            uint8_t ipublish:1;
            uint8_t ping:1;
            struct {
                mqtt_subs_opts_t *opts;
                uint32_t num;
            } subscribe;
        } pending;
    } session;
} mqtt_client_t;


int mqtt_connect(mqtt_client_t *c, mqtt_connect_opts_t *opts, bool *session_present);
int mqtt_disconnect(mqtt_client_t *c);
int mqtt_publish(mqtt_client_t *c, mqtt_msg_t *msg);
int mqtt_subscribe(mqtt_client_t *c, mqtt_subs_opts_t *opts, uint32_t num);
int mqtt_unsubscribe(mqtt_client_t *c, char *topics[], uint32_t num);
int mqtt_loop(mqtt_client_t *c);

#endif /* _MQTT_H */

/* vim: set ts=4 sw=4 sts=4 et */


