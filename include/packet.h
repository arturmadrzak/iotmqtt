#ifndef _PACKET_H
#define _PACKET_H

#define MQTT_VERSION_311 0x04

#define CONFIG_MQTT_LEVEL MQTT_VERSION_311

#include "frame.h"

enum qos {
    QoS0,
    QoS1,
    QoS2
};

enum packet_type {
    RESERVED_BEGIN,
    CONNECT,
    CONNACK,
    PUBLISH,
    PUBACK,
    PUBREC,
    PUBREL,
    PUBCOMP,
    SUBSCRIBE,
    SUBACK,
    UNSUBSCRIBE,
    UNSUBACK,
    PINGREQ,
    PINGRESP,
    DISCONNECT,
    RESERVED_END
};

enum conn_return_code {
    CONN_ACCEPTED,
    CONN_REFUSED_WRONG_PROTO_VERSION,
    CONN_REFUSED_WRONG_ID,
    CONN_REFUSED_SERVER_UNAVAILABLE,
    CONN_REFUSED_BAD_USERNAME_OR_PASSWORD,
    CONN_REFUSED_NOT_AUTHORIZED
};

enum suback_return_code {
    SUBS_FAILED = 0x80
};
typedef uint8_t suback_rc_t;

typedef struct {
    char* topic;
    uint8_t* msg;
    uint16_t mlen;
    uint8_t retain : 1;
    uint8_t qos : 2;
} mqtt_msg_t;

typedef struct  {
    const char* client_id;
    const char* username;
    const char* password;
    uint16_t keep_alive;
    uint8_t clean_session:1;
    mqtt_msg_t *will;
} mqtt_connect_opts_t;
#define mqtt_connect_opts_defaults { .client_id = NULL, \
    .username = NULL, \
    .password = NULL, \
    .keep_alive = 60, \
    .clean_session = 1, \
    .will = NULL }

typedef struct {
    uint8_t session_present;
    enum conn_return_code return_code;
} mqtt_connack_opts_t;

typedef struct {
    char *topic;
    enum qos qos;
} mqtt_subs_opts_t;

typedef struct {
    uint32_t num;
    enum suback_return_code rc[];
} mqtt_suback_opts_t;

int out_packet_connect(mqtt_connect_opts_t *opts, mqtt_frame_t *desc);
int out_packet_publish(uint16_t packet_id, mqtt_msg_t *msg, mqtt_frame_t *desc);
int out_packet_puback(uint16_t packet_id, mqtt_frame_t *desc);
int out_packet_pubrec(uint16_t packet_id, mqtt_frame_t *desc);
int out_packet_pubrel(uint16_t packet_id, mqtt_frame_t *desc);
int out_packet_pubcomp(uint16_t packet_id, mqtt_frame_t *desc);
int out_packet_subscribe(uint16_t packet_id, mqtt_subs_opts_t *opts, uint32_t num, mqtt_frame_t *desc);
int out_packet_unsubscribe(uint16_t packet_id, char *topics[], uint32_t num, mqtt_frame_t *desc);
int out_packet_pingreq(mqtt_frame_t *desc);
int out_packet_disconnect(mqtt_frame_t *desc);


/* @{
 ** In packets documentation
 * @param desc [in] read frame descriptor
 * @return packet id if returned value is non negative
 *         @mqtt_errno otherwise
 */

/**
 * @param opts [out] 
 * @return 0 if successfully connected to the remote host
 *         negative @mqtt_errno otherwise
 */
int in_packet_connack(mqtt_frame_t* desc, mqtt_connack_opts_t *opts);

/**
 * @param msg [out] message published by remote host
 */
int in_packet_publish(mqtt_frame_t* desc, mqtt_msg_t *msg);

int in_packet_pub_response(mqtt_frame_t *desc);
int in_packet_puback(mqtt_frame_t* desc);
int in_packet_pubrec(mqtt_frame_t* desc);
int in_packet_pubrel(mqtt_frame_t* desc);
int in_packet_pubcomp(mqtt_frame_t* desc);

/**
 * @param rcodes [in/out] list of return codes corresponding to subscribed topics.
 *                        At the end of the list is placed magic value 0xFF.
 * @param num [out] number of received error codes
 */
int in_packet_suback(mqtt_frame_t* desc, uint32_t max, suback_rc_t rcodes[], uint32_t *num);

int in_packet_unsuback(mqtt_frame_t* desc);
int in_packet_pingresp(mqtt_frame_t* desc);

/* @} */

#endif /* _PACKET_H */

/* vim: set ts=4 sw=4 sts=4 et */

