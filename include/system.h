#ifndef _SYSTEM_H
#define _SYSTEM_H

#include <stdint.h>


/*
 * @return unsettable clock in seconds
 */
uint32_t mqtt_clock(void);


typedef void* mqtt_io_t;
int mqtt_io_write(mqtt_io_t *fd, uint8_t *buffer, uint32_t len, uint32_t timeout);
int mqtt_io_read(mqtt_io_t *fd, uint8_t *buffer, uint32_t len, uint32_t timeout);

#endif /* _SYSTEM_H */

