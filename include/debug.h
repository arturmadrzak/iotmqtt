#ifndef _DEBUG_H
#define _DEBUG_H

#include <stdio.h>
#include <string.h>
#include "mqtt.h"

typedef enum
{
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG,
} log_level_t;

const char* mqtt_errnostr(int errno);

#if NDEBUG || UNIT_TEST

#define log_error(format, ...)
#define log_warning(format, ...)
#define log_info(format, ...)
#define log_debug(format, ...)

#define log_mqtt_frame(x,y)
#define mqtt_errnostr(x)

#else

#define log_error(format, ...)   dlog_print(LOG_ERROR,   __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#define log_warning(format, ...) dlog_print(LOG_WARNING, __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#define log_info(format, ...)    dlog_print(LOG_INFO,    __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#define log_debug(format, ...)   dlog_print(LOG_DEBUG,   __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)

#define dlog_print(level, file, func, line, format, ...) \
{\
    if (level <= current_log_level) \
    { \
        printf("[%s]%d:%s():", level_desc[level], line, func); \
        printf(format, ##__VA_ARGS__); \
        if (format[strlen(format)-1] != '\n') \
        { \
            printf("\n"); \
        } \
    } \
}

extern log_level_t current_log_level;
extern const char *level_desc[];

void log_mqtt_frame(const char *dir, mqtt_frame_t *desc);
#endif

#endif /* _DEBUG_H */

