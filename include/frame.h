#ifndef _FRAME_H
#define _FRAME_H

#include <stdint.h>

typedef union {
    uint8_t byte;
    struct {
        uint8_t retain : 1;
        uint8_t qos : 2;
        uint8_t dup : 1;
        uint8_t type : 4;
    } bits;
} mqtt_header_t;

typedef struct  {
    mqtt_header_t   header;
    uint32_t        plen;
    uint8_t         payload[];
} mqtt_frame_t;

int mqtt_frame_serialize(mqtt_io_t *fd, mqtt_frame_t *desc, uint32_t deadline);
int mqtt_frame_deserialize(mqtt_io_t *fd, mqtt_frame_t *desc, uint32_t size, uint32_t deadline);

#endif /* _FRAME_H */

/* vim: set ts=4 sw=4 sts=4 et */

