# iotmqtt, MQTT v3.1.1 compliant library

```
Development still in progress. Not ready for production use!
```

iotmqtt is lightweight and protable implementation of client side MQTT v3.1.1 protocol specification. I wrote it because I didn't find any open source library meeting my requirements.

## Features
* single threaded
* no internal memory allocation
* tested
* portable
* lightweight
* no extra dependencies
* c99 compliant
* posix compliant

## Examples
For working example see ``sample`` sources.

## Things to be done
* buffer length checking in packet.c
* clean up headers, sources
* maybe prepare augmented sources -> merge headers and sources for easier integration
* refactor and cleanup frame and packet unit tests
* finish client tests
* prepare documentation
* write tests for external api (for system.h functions)
* clean up formatting, legal headers etc.

## Licensing
iotmqtt is open source software and is licensed under MIT License; 
see ``LICENSE`` file for mor details.

## Author
iotmqtt was written by Artur Mądrzak &lt;artur&commat;madrzak.eu&gt;
