#ifndef _NET_H
#define _NET_H

struct net;
typedef struct net net_t;

struct net {
    int (*connect)(net_t* net, char *ip, int port);
    int (*mqttwrite)(net_t* net, unsigned char *buffer, int len, int timeout_ms);
    int (*mqttread)(net_t* net, unsigned char *buffer, int len, int timeout_ms);
    void (*disconnect)(net_t* net);
    int sock;
};

net_t* net_alloc();

#endif /* _NET_H */
