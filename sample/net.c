#include "net.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <mqtt.h>

static int m_connect(net_t *net, char *ip, int port)
{
    struct sockaddr_in addr;
    if (net->sock >= 0)
    {
        /* already connected or uninitialized */
        printf("%s:%d Already connected\n", __FUNCTION__, __LINE__);
        return -1;
    }

    if ((net->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("%s:%d Can't create socket\n", __FUNCTION__, __LINE__);
        return -1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip);

    if(connect(net->sock, (struct sockaddr*)&addr, sizeof(addr)) < 0)
    {
        printf("%s:%d Can't connect to: %s:%d\n", __FUNCTION__, __LINE__, ip, port);
        printf("%d:%s\n", errno, strerror(errno));
        net->disconnect(net); /* close socket, cleanup */
        return -1;
    }
    return 0;
}

void m_disconnect(net_t *net)
{
    close(net->sock);
    net->sock = -1;
}

static int m_write(net_t* net, unsigned char* buffer, int len, int timeout_ms)
{
    struct timeval timeout;
    timeout.tv_sec = timeout_ms / 1000;
    timeout.tv_usec = (timeout_ms % 1000) * 1000;
    if (setsockopt(net->sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)) < 0)
    {
        printf("Setting send timeout failed.\n");
        printf("%d:%s\n", errno, strerror(errno));
        return -1;
    }
    return write(net->sock, buffer, len);
}

static int m_read(net_t* net, unsigned char* buffer, int len, int timeout_ms)
{
    int rv;
    struct timeval timeout;
    timeout.tv_sec = timeout_ms / 1000;
    timeout.tv_usec = (timeout_ms % 1000) * 1000;

    if (setsockopt(net->sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
    {
        printf("Setting receive timeout failed.\n");
        return -1;
    }
    rv = read(net->sock, buffer, len);
    if (rv <= 0) {
        if (errno == EAGAIN)
            return MQTT_ERRNO_TIMEOUT;
        else
            return MQTT_ERRNO_BROKEN_PIPE;
    }
    return rv;
}

net_t* net_alloc()
{
    net_t *net = malloc(sizeof(net_t));
    if (!net) 
    {
        return NULL;
    }
    net->connect = m_connect;
    net->mqttread = m_read;
    net->mqttwrite = m_write;
    net->disconnect = m_disconnect;
    net->sock = -1;
    return net;
}

