#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "net.h"
#include "mqtt.h"
#include "debug.h"

static bool keep_running = true;
static unsigned char iobuf[512];

static int handle_message(mqtt_msg_t *msg)
{
    printf("Received message: |%s|\n", msg->topic);
    printf("\tmsg: %s\n", msg->msg);
    return MQTT_ERRNO_OK;
}

static int connect(mqtt_client_t *c)
{
    bool session_present = false;
    mqtt_connect_opts_t opts = mqtt_connect_opts_defaults;

    opts.client_id = "iotmqtt-82b1c621";
    opts.clean_session = true;
    opts.keep_alive = 60;
   
    return mqtt_connect(c, &opts, &session_present);
}

static int publish(mqtt_client_t *c)
{
    mqtt_msg_t msg;

    msg.qos = QoS0;
    msg.retain = false;
    msg.topic = "madry/iotmqtt";
    msg.msg = "welcome";
    msg.mlen = strlen(msg.msg);
    return mqtt_publish(c, &msg);
}

static int subscribe(mqtt_client_t *c)
{
    mqtt_subs_opts_t opts;
    opts.topic = "madry/iotmqtt/listen";
    opts.qos = QoS1;
    return mqtt_subscribe(c, &opts, 1);
}

static void do_some_sample(mqtt_client_t *c)
{
    int rv;
    uint32_t deadline, now;
    
    if ((rv = connect(c))) {
        printf("Connection refused: %s\n", mqtt_errnostr(rv));
        return;
    }
    
    if ((rv = publish(c))) {
        printf("Publishing failed: %s\n", mqtt_errnostr(rv));
        /* don't exit. */
    }

    if ((rv = subscribe(c))) {
        printf("Subscribing failed: %s\n", mqtt_errnostr(rv));
        /* don't exit. */
    }

    printf("Listen for subscriptions: %u\n", mqtt_clock());
    deadline = mqtt_clock() + 300;
    do {
        rv = mqtt_loop(c);
        now = mqtt_clock();
        if (!keep_running || rv != MQTT_ERRNO_TIMEOUT && rv != MQTT_ERRNO_OK)
            break;
    } while(now < deadline);

    printf("Done: %d. Time to rest: %u\n", rv, mqtt_clock());

    if ((rv = mqtt_disconnect(c))) {
        printf("Disconnection error: %s\n", mqtt_errnostr(rv));
        return;
    }
    printf("Successfully disconnected\n");
}

static void sigint_handler(int signum)
{
    keep_running = false;
}

int main(int argc, char* argv[])
{
    signal(SIGINT, sigint_handler);
    printf("IoTMqtt library sample\n");

    mqtt_client_t* c;
    c = calloc(1, sizeof(mqtt_client_t));
    c->buffer.desc = (mqtt_frame_t*) iobuf;
    c->buffer.size = sizeof(iobuf);
    c->input_msg.topic = calloc(1, 1024);
    c->input_msg.msg = calloc(1, 1024);
    c->timeout = 10;
    c->callback = handle_message;

    if(!mqtt_io_open(&c->fd, "127.0.0.1", 1883))   //,198.41.30.241 "85.119.83.194"
    {
        do_some_sample(c);
        mqtt_io_close(&c->fd);
    } else {
        printf("Establishing IP connection FAILED\n");
    }
    free(c->input_msg.topic);
    free(c->input_msg.msg);
    free(c);

    return 0;
}

