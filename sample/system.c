#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include "mqtt.h"

uint32_t mqtt_clock(void)
{
    struct timespec tp;
    if (clock_gettime(CLOCK_MONOTONIC, &tp)) {
        fprintf(stderr, "Can't get time\n");
        return 0;
    }
    return tp.tv_sec;
}

int mqtt_io_open(mqtt_io_t *fd, char *ip, int port) 
{
    int *sock = (int*)fd;
    struct sockaddr_in addr;
    if (*sock >= 0)
    {
        /* already connected or uninitialized */
        printf("%s:%d Already connected\n", __FUNCTION__, __LINE__);
        return -1;
    }

    if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("%s:%d Can't create socket\n", __FUNCTION__, __LINE__);
        return -1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip);

    if(connect(*sock, (struct sockaddr*)&addr, sizeof(addr)) < 0)
    {
        printf("%s:%d Can't connect to: %s:%d\n", __FUNCTION__, __LINE__, ip, port);
        printf("%d:%s\n", errno, strerror(errno));
        mqtt_io_close(fd);
        return -1;
    }
    return 0;

}

int mqtt_io_close(mqtt_io_t *fd)
{
    int *sock = (int*)fd;
    close(*sock);
    *sock = -1;
}

int mqtt_io_write(mqtt_io_t *fd, uint8_t *buffer, uint32_t len, uint32_t timeout)
{
    int *sock = (int*)fd;
    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 0; 
    if (setsockopt(*sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0)
    {
        printf("Setting send timeout failed.\n");
        printf("%d:%s\n", errno, strerror(errno));
        return -1;
    }
    return write(*sock, buffer, len);
}

int mqtt_io_read(mqtt_io_t *fd, uint8_t *buffer, uint32_t len, uint32_t timeout)
{
    int *sock = (int*)fd;
    int rv;
    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    if (setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        printf("Setting receive timeout failed.\n");
        return -1;
    }
    rv = read(*sock, buffer, len);
    if (rv <= 0) {
        if (errno == EAGAIN)
            return MQTT_ERRNO_TIMEOUT;
        else
            return MQTT_ERRNO_BROKEN_PIPE;
    }
    return rv;
}

