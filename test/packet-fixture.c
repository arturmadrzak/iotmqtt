#include "unity.h"
#include "unity_fixture.h"
#include "mqtt.h"
#include <unistd.h>
#include <string.h>

#define TEST_HeaderPacketType(group, func, ptype) \
    TEST(group, HeaderPacketType) \
    { \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, func); \
        TEST_ASSERT_EQUAL(ptype, g_desc->header.bits.type); \
    }

#define TEST_HeaderPacketLength(group, func, len) \
    TEST(group, HeaderPacketLength) \
    { \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, func); \
        TEST_ASSERT_EQUAL(len, g_desc->plen); \
    }

#define TEST_HeaderPacketQoS(group, func, _qos)  \
    TEST(group, HeaderPacketQoS) \
    { \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, func); \
        TEST_ASSERT_EQUAL(_qos, g_desc->header.bits.qos); \
    }

#define TEST_HeaderPacketDup(group, func, _dup)  \
    TEST(group, HeaderPacketDup) \
    { \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, func); \
        TEST_ASSERT_EQUAL(_dup, g_desc->header.bits.dup); \
    }

#define TEST_HeaderPacketRetain(group, func, _retain)  \
    TEST(group, HeaderPacketRetain) \
    { \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, func); \
        TEST_ASSERT_EQUAL(_retain, g_desc->header.bits.retain); \
    }

#define TEST_PacketIdOnPayloadBegin(group, func, _pktid)  \
    TEST(group, PacketIdOnPayloadBegin) \
    { \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, func); \
        TEST_ASSERT_EQUAL_HEX16(swap16(_pktid), *(uint16_t*)g_desc->payload); \
    }

#define TEST_InInvalidHeaderPacketType(group, func) \
    TEST(group, InvalidHeaderPacketType) \
    { \
        g_desc->header.bits.type = CONNECT; \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, func); \
    }

#define TEST_InInvalidHeaderDup(group, func) \
    TEST(group, InvalidHeaderDup) \
    { \
        g_desc->header.bits.dup = 1; \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, func); \
    }

#define TEST_InInvalidHeaderQoS(group, func, _qos) \
    TEST(group, InvalidHeaderQoS) \
    { \
        g_desc->header.bits.qos = _qos; \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, func); \
    }

#define TEST_InInvalidHeaderRetain(group, func) \
    TEST(group, InvalidHeaderRetain) \
    { \
        g_desc->header.bits.retain = 1; \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, func); \
    }

#define TEST_InInvalidPayloadLength(group, func, _len) \
    TEST(group, InvalidPayloadLength) \
    { \
        g_desc->plen = _len; \
        TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, func); \
    }

static char g_topic[] = "topic/example";
static mqtt_frame_t* g_desc;
static mqtt_connect_opts_t g_opts;
static mqtt_msg_t *g_msg;
/* ========================================================================================= */

TEST_GROUP(out_packet_connect);
TEST_SETUP(out_packet_connect)
{
    mqtt_connect_opts_t defaults = mqtt_connect_opts_defaults;
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
    g_opts = defaults;
    g_opts.client_id = "mqtt-client";
}

TEST_TEAR_DOWN(out_packet_connect)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_connect, out_packet_connect(&g_opts, g_desc), CONNECT);
TEST_HeaderPacketLength(out_packet_connect, out_packet_connect(&g_opts, g_desc), 23);
TEST_HeaderPacketQoS(out_packet_connect, out_packet_connect(&g_opts, g_desc), 0);
TEST_HeaderPacketDup(out_packet_connect, out_packet_connect(&g_opts, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_connect, out_packet_connect(&g_opts, g_desc), 0);

TEST(out_packet_connect, ProtocolNameLength)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(4), *(uint16_t*)g_desc->payload);
}

TEST(out_packet_connect, ProtocolName)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_STRING_LEN("MQTT", (char*)&g_desc->payload[2], 4);
}

TEST(out_packet_connect, ProtocolLevel)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX8(4, g_desc->payload[6]);
}

TEST(out_packet_connect, KeepAliveOnTwoBytes)
{
    g_opts.keep_alive = 0xFFFA;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(g_opts.keep_alive), *(uint16_t*)&g_desc->payload[8]);
}

TEST(out_packet_connect, ClientId)
{
    uint16_t expected_cid_len = strlen(g_opts.client_id);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(expected_cid_len), *(uint16_t*)&g_desc->payload[10]);
    TEST_ASSERT_EQUAL_STRING_LEN(g_opts.client_id, (char*)&g_desc->payload[12], expected_cid_len);
}

TEST(out_packet_connect, ClientIdTooLong)
{
    g_opts.client_id = "012345678901234567891234";
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_CLIENT_ID, out_packet_connect(&g_opts, g_desc));
}

/* ========================================================================================= */
TEST_GROUP(out_packet_connect_flags);

char g_conn_hdr[] = "\x0\x4MQTT\x4\x0\x0\x3c";
char* g_conn_flg = &g_conn_hdr[7];


TEST_SETUP(out_packet_connect_flags)
{
    mqtt_connect_opts_t defaults = mqtt_connect_opts_defaults;
    g_desc = malloc(1024);
    memset(g_desc, 0xaa, 1024);
    g_opts = defaults;
    g_opts.client_id = "mqtt-client";
    g_opts.clean_session = 0;
}

TEST_TEAR_DOWN(out_packet_connect_flags)
{
    free(g_desc);
}

TEST(out_packet_connect_flags, ReservedFlag)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL(0, g_desc->payload[7] & 0x01);
}

TEST(out_packet_connect_flags, UserNameFlag)
{
    g_opts.username = "username";
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL(1, (g_desc->payload[7] & 0x80) != 0);
}

TEST(out_packet_connect_flags, PasswordFlagWhenNoUserFlag)
{
    g_opts.password = "password";
    TEST_ASSERT_EQUAL(MQTT_ERRNO_USERNAME_REQUIRED, out_packet_connect(&g_opts, g_desc));
}

TEST(out_packet_connect_flags, PasswordFlag)
{
    g_opts.username = "username";
    g_opts.password = "password";
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX8(0xC0, g_desc->payload[7]);
}

TEST(out_packet_connect_flags, WillFlag)
{
    mqtt_msg_t will = {
        .topic = g_topic,
        .msg = NULL
    };
    g_opts.will = &will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX8(0x04, g_desc->payload[7]);
}

TEST(out_packet_connect_flags, WillQoS1)
{
    mqtt_msg_t will = {
        .topic = g_topic,
        .msg = NULL
    };
    will.qos = 1;
    g_opts.will = &will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX8(0x0C, g_desc->payload[7]);
}

TEST(out_packet_connect_flags, WillQoS2)
{
    mqtt_msg_t will = {
        .topic = g_topic,
        .msg = NULL
    };
    will.qos = 2;
    g_opts.will = &will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX8(0x14, g_desc->payload[7]);
}

TEST(out_packet_connect_flags, InvalidWillQoS)
{
    mqtt_msg_t will;
    will.qos = 3;
    g_opts.will = &will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_QOS, out_packet_connect(&g_opts, g_desc));
}

TEST(out_packet_connect_flags, WillRetain)
{
    mqtt_msg_t will = {
        .topic = g_topic,
        .msg = NULL
    };
    will.retain = 1;
    g_opts.will = &will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX8(0x24, g_desc->payload[7]);
}

TEST(out_packet_connect_flags, CleanSession)
{
    g_opts.clean_session = 1;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL(0x2, g_desc->payload[7]);

}

/* ========================================================================================= */

TEST_GROUP(out_packet_connect_optional_payload);
#define OFFSET (10 + 2 + strlen(g_opts.client_id))
mqtt_msg_t g_will;
TEST_SETUP(out_packet_connect_optional_payload)
{
    mqtt_connect_opts_t defaults = mqtt_connect_opts_defaults;
    g_desc = malloc(1024);
    memset(g_desc, 0, 1024);
    g_opts = defaults;
    g_opts.client_id = "mqtt-client";
    memset(&g_will, 0, sizeof(mqtt_msg_t));
}

TEST_TEAR_DOWN(out_packet_connect_optional_payload)
{
    free(g_desc);
}
TEST(out_packet_connect_optional_payload, WillTopicField)
{
    uint16_t expected_will_len;
    g_will.topic = "topic/test";
    expected_will_len = strlen(g_will.topic);
    g_opts.will = &g_will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(expected_will_len), *(uint16_t*)&g_desc->payload[OFFSET]);
    TEST_ASSERT_EQUAL_STRING(g_will.topic, (char*)&g_desc->payload[OFFSET + 2]);
}

TEST(out_packet_connect_optional_payload, WillMessageField)
{
    uint16_t expected_will_len;
    g_will.topic = "topic/test";
    g_will.msg = (uint8_t*)"test payload";
    g_will.mlen = strlen((const char*)g_will.msg);
    expected_will_len = strlen(g_will.topic);
    g_opts.will = &g_will;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(g_will.mlen), *(uint16_t*)&g_desc->payload[OFFSET + 2 + expected_will_len]);
    TEST_ASSERT_EQUAL_STRING(g_will.msg, (char*)&g_desc->payload[OFFSET + 2 + expected_will_len + 2]);
}

TEST(out_packet_connect_optional_payload, UserNameField)
{
    uint16_t expected_username_len;
    g_opts.username = "username";
    expected_username_len = strlen(g_opts.username);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(expected_username_len), *(uint16_t*)&g_desc->payload[OFFSET]);
    TEST_ASSERT_EQUAL_STRING(g_opts.username, (char*)&g_desc->payload[OFFSET + 2]);
}

TEST(out_packet_connect_optional_payload, PasswordField)
{
    uint16_t expected_username_len;
    uint16_t expected_password_len;
    g_opts.username = "username";
    g_opts.password = "password";
    expected_username_len = strlen(g_opts.username);
    expected_password_len = strlen(g_opts.password);

    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_connect(&g_opts, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(expected_password_len), *(uint16_t*)&g_desc->payload[OFFSET + 2 + expected_username_len]);
    TEST_ASSERT_EQUAL_STRING(g_opts.password, (char*)&g_desc->payload[OFFSET + 2 + expected_username_len + 2]);
}

TEST(out_packet_connect_optional_payload, PasswordFieldExistsButNoUserName)
{
    g_opts.password = "password";
    TEST_ASSERT_EQUAL(MQTT_ERRNO_USERNAME_REQUIRED, out_packet_connect(&g_opts, g_desc));
}



/* ========================================================================================= */

TEST_GROUP(out_packet_publish);

char g_payload[] = "example paylad";
TEST_SETUP(out_packet_publish)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
    g_msg = malloc(sizeof(mqtt_msg_t));
    memset(g_msg, 0, sizeof(mqtt_msg_t));
    g_msg->msg = (uint8_t*)g_payload;
    g_msg->mlen = strlen(g_payload);
    g_msg->topic = g_topic;
}

TEST_TEAR_DOWN(out_packet_publish)
{
    free(g_desc);
    free(g_msg);
}

TEST_HeaderPacketType(out_packet_publish, out_packet_publish(0, g_msg, g_desc), PUBLISH);
TEST_HeaderPacketLength(out_packet_publish, out_packet_publish(0, g_msg, g_desc), 29);
TEST_HeaderPacketQoS(out_packet_publish, out_packet_publish(0, g_msg, g_desc), 0);
TEST_HeaderPacketDup(out_packet_publish, out_packet_publish(0, g_msg, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_publish, out_packet_publish(0, g_msg, g_desc), 0);


TEST(out_packet_publish, CheckQoS2)
{
    g_msg->qos = QoS2;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0, g_msg, g_desc));
    TEST_ASSERT_EQUAL(QoS2, g_desc->header.bits.qos);
}

TEST(out_packet_publish, CheckWrongQoS)
{
    g_msg->qos = 3;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_QOS, out_packet_publish(0, g_msg, g_desc));
}

TEST(out_packet_publish, CheckRetain)
{
    g_msg->retain = 1;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0, g_msg, g_desc));
    TEST_ASSERT_EQUAL(1, g_desc->header.bits.retain);
}

TEST(out_packet_publish, CheckTopicName)
{
    uint16_t expected_topic_len = strlen(g_topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0, g_msg, g_desc));
    TEST_ASSERT_EQUAL(swap16(expected_topic_len), *(uint16_t*)g_desc->payload);
    TEST_ASSERT_EQUAL_STRING_LEN(g_topic, (char*)&g_desc->payload[2], expected_topic_len);
}

TEST(out_packet_publish, CheckPacketIdIsAbsentWithQoS0)
{
    uint16_t expected_topic_len = strlen(g_topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0, g_msg, g_desc));
    TEST_ASSERT_EQUAL(2 + expected_topic_len + strlen(g_payload), g_desc->plen);
}

TEST(out_packet_publish, CheckPacketIdWithQoS2)
{
    uint16_t expected_topic_len = strlen(g_topic);
    g_msg->qos = QoS2;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0xa00a, g_msg, g_desc));
    TEST_ASSERT_EQUAL(swap16(0xa00a), *(uint16_t*)&g_desc->payload[2 + expected_topic_len]);
}

TEST(out_packet_publish, CheckPayloadWithQoS0)
{
    uint16_t expected_topic_len = strlen(g_topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0x0, g_msg, g_desc));
    TEST_ASSERT_EQUAL_STRING_LEN(g_payload, (char*)&g_desc->payload[2 + expected_topic_len], strlen(g_payload));
}

TEST(out_packet_publish, CheckPayloadWithQoS2)
{
    uint16_t expected_topic_len = strlen(g_topic);
    g_msg->qos = QoS2;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0x0, g_msg, g_desc));
    TEST_ASSERT_EQUAL_STRING_LEN(g_payload, (char*)&g_desc->payload[2 + expected_topic_len + 2], strlen(g_payload));
}

TEST(out_packet_publish, ZeroLengthMessage)
{
    uint16_t expected_topic_len = strlen(g_topic);
    g_msg->mlen = 0;
    g_msg->msg = NULL;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_publish(0x0, g_msg, g_desc));
    TEST_ASSERT_EQUAL(2 + expected_topic_len, g_desc->plen);

}

/* ========================================================================================= */

TEST_GROUP(out_packet_puback);
TEST_SETUP(out_packet_puback)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_puback)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_puback, out_packet_puback(0, g_desc), PUBACK);
TEST_HeaderPacketLength(out_packet_puback, out_packet_puback(0, g_desc), 2);
TEST_HeaderPacketQoS(out_packet_puback, out_packet_puback(0, g_desc), 0);
TEST_HeaderPacketDup(out_packet_puback, out_packet_puback(0, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_puback, out_packet_puback(0, g_desc), 0);
TEST_PacketIdOnPayloadBegin(out_packet_puback, out_packet_puback(0xabcd, g_desc), 0xabcd);

/* ========================================================================================= */

TEST_GROUP(out_packet_pubrec);
TEST_SETUP(out_packet_pubrec)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_pubrec)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_pubrec, out_packet_pubrec(0, g_desc), PUBREC);
TEST_HeaderPacketLength(out_packet_pubrec, out_packet_pubrec(0, g_desc), 2);
TEST_HeaderPacketQoS(out_packet_pubrec, out_packet_pubrec(0, g_desc), 0);
TEST_HeaderPacketDup(out_packet_pubrec, out_packet_pubrec(0, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_pubrec, out_packet_pubrec(0, g_desc), 0);
TEST_PacketIdOnPayloadBegin(out_packet_pubrec, out_packet_pubrec(0xabcd, g_desc), 0xabcd);

/* ========================================================================================= */

TEST_GROUP(out_packet_pubrel);
TEST_SETUP(out_packet_pubrel)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_pubrel)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_pubrel, out_packet_pubrel(0, g_desc), PUBREL);
TEST_HeaderPacketLength(out_packet_pubrel, out_packet_pubrel(0, g_desc), 2);
TEST_HeaderPacketQoS(out_packet_pubrel, out_packet_pubrel(0, g_desc), QoS1);
TEST_HeaderPacketDup(out_packet_pubrel, out_packet_pubrel(0, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_pubrel, out_packet_pubrel(0, g_desc), 0);
TEST_PacketIdOnPayloadBegin(out_packet_pubrel, out_packet_pubrel(0xabcd, g_desc), 0xabcd);

/* ========================================================================================= */

TEST_GROUP(out_packet_pubcomp);
TEST_SETUP(out_packet_pubcomp)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_pubcomp)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_pubcomp, out_packet_pubcomp(0, g_desc), PUBCOMP);
TEST_HeaderPacketLength(out_packet_pubcomp, out_packet_pubcomp(0, g_desc), 2);
TEST_HeaderPacketQoS(out_packet_pubcomp, out_packet_pubcomp(0, g_desc), 0);
TEST_HeaderPacketDup(out_packet_pubcomp, out_packet_pubcomp(0, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_pubcomp, out_packet_pubcomp(0, g_desc), 0);
TEST_PacketIdOnPayloadBegin(out_packet_pubcomp, out_packet_pubcomp(0xabcd, g_desc), 0xabcd);

/* ========================================================================================= */

TEST_GROUP(out_packet_subscribe);
static mqtt_subs_opts_t *g_subs_list;

TEST_SETUP(out_packet_subscribe)
{
    int i;
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
    g_subs_list = malloc(10 * sizeof(mqtt_subs_opts_t));
    for(i = 0; i < 10; i++) {
        g_subs_list[i].topic = g_topic;
        g_subs_list[i].qos = (i % 3);
    }
}

TEST_TEAR_DOWN(out_packet_subscribe)
{
    free(g_desc);
    free(g_subs_list);
}

TEST_HeaderPacketType(out_packet_subscribe, out_packet_subscribe(0, g_subs_list, 1, g_desc), SUBSCRIBE);
/* expected len: packet_id(2) + topic_len(2) + topic(13) + qos(1) */
TEST_HeaderPacketLength(out_packet_subscribe, out_packet_subscribe(0, g_subs_list, 1, g_desc), 2+2+13+1);
TEST_HeaderPacketQoS(out_packet_subscribe, out_packet_subscribe(0, g_subs_list, 1, g_desc), QoS1);
TEST_HeaderPacketDup(out_packet_subscribe, out_packet_subscribe(0, g_subs_list, 1, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_subscribe, out_packet_subscribe(0, g_subs_list, 1, g_desc), 0);

TEST(out_packet_subscribe, PacketId)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_subscribe(0x1234, g_subs_list, 1, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(0x1234), *(uint16_t*)g_desc->payload);
}

TEST(out_packet_subscribe, EmptyList)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_INVALID_VALUE, out_packet_subscribe(0x1234, NULL, 0, g_desc));
}

TEST(out_packet_subscribe, OneTopicLength)
{
    uint16_t expected_len = (uint16_t)strlen(g_subs_list[0].topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_subscribe(0, g_subs_list, 1, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(expected_len), *(uint16_t*)&g_desc->payload[2]);
}

TEST(out_packet_subscribe, OneTopicValue)
{
    uint16_t expected_len = (uint16_t)strlen(g_subs_list[0].topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_subscribe(0, g_subs_list, 1, g_desc));
    TEST_ASSERT_EQUAL_STRING_LEN(g_topic, &g_desc->payload[2 + 2], expected_len);
}

TEST(out_packet_subscribe, OneTopicQoS)
{
    uint16_t expected_len = (uint16_t)strlen(g_subs_list[0].topic);
    g_subs_list[0].qos = QoS2;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_subscribe(0, g_subs_list, 1, g_desc));
    TEST_ASSERT_EQUAL_HEX8(2, g_desc->payload[2 + 2 + expected_len]);
}

TEST(out_packet_subscribe, TopicListValues)
{
    int i;
    uint16_t expected_len = (uint16_t)strlen(g_subs_list[0].topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_subscribe(0, g_subs_list, 5, g_desc));
    for(i = 0; i < 5; i++) {
        TEST_ASSERT_EQUAL_STRING_LEN(g_topic, &g_desc->payload[2 + 2 + i*(2 + 1 + expected_len)], expected_len);
    }
}

TEST(out_packet_subscribe, TopicListQoS)
{
    int i;
    uint16_t expected_len = (uint16_t)strlen(g_subs_list[0].topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_subscribe(0, g_subs_list, 5, g_desc));
    for(i = 0; i < 5; i++) {
        TEST_ASSERT_EQUAL((i % 3), g_desc->payload[2 + 2 + expected_len + i*(2 + 1 + expected_len)]);
    }
}

TEST(out_packet_subscribe, NullTopicName)
{
    g_subs_list[2].topic = NULL;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_INVALID_VALUE, out_packet_subscribe(0, g_subs_list, 5, g_desc));
}

TEST(out_packet_subscribe, InvalidQoS)
{
    g_subs_list[2].qos = 3;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_INVALID_VALUE, out_packet_subscribe(0, g_subs_list, 5, g_desc));
}

/* ========================================================================================= */

TEST_GROUP(out_packet_unsubscribe);
static char *g_unsubs_topics[] = {
    g_topic,
    g_topic,
    g_topic
};
TEST_SETUP(out_packet_unsubscribe)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_unsubscribe)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_unsubscribe, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc), UNSUBSCRIBE);
/* expected len: packet_id(2) + topic_len(2) + topic(13) */
TEST_HeaderPacketLength(out_packet_unsubscribe, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc), 17);
TEST_HeaderPacketQoS(out_packet_unsubscribe, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc), QoS1);
TEST_HeaderPacketDup(out_packet_unsubscribe, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc), 0);
TEST_HeaderPacketRetain(out_packet_unsubscribe, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc), 0);

TEST(out_packet_unsubscribe, PacketId)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_unsubscribe(0x1234, g_unsubs_topics, 1, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(0x1234), *(uint16_t*)g_desc->payload);
}

TEST(out_packet_unsubscribe, EmptyList)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_INVALID_VALUE, out_packet_unsubscribe(0, NULL, 0, g_desc));
}

TEST(out_packet_unsubscribe, OneTopicLength)
{
    uint16_t expected_len = (uint16_t)strlen(g_unsubs_topics[0]);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc));
    TEST_ASSERT_EQUAL_HEX16(swap16(expected_len), *(uint16_t*)&g_desc->payload[2]);
}

TEST(out_packet_unsubscribe, OneTopicValue)
{
    uint16_t expected_len = (uint16_t)strlen(g_topic);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_unsubscribe(0, g_unsubs_topics, 1, g_desc));
    TEST_ASSERT_EQUAL_STRING_LEN(g_topic, &g_desc->payload[2 + 2], expected_len);
}

TEST(out_packet_unsubscribe, TopicListValues)
{
    int i;
    uint16_t expected_len = (uint16_t)strlen(g_unsubs_topics[0]);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, out_packet_unsubscribe(0, g_unsubs_topics, 3, g_desc));
    for(i = 0; i < 3; i++) {
        TEST_ASSERT_EQUAL_STRING_LEN(g_topic, &g_desc->payload[2 + 2 + i*(2 + expected_len)], expected_len);
    }
}

/* ========================================================================================= */
TEST_GROUP(out_packet_pingreq);

TEST_SETUP(out_packet_pingreq)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_pingreq)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_pingreq, out_packet_pingreq(g_desc), PINGREQ);
TEST_HeaderPacketLength(out_packet_pingreq, out_packet_pingreq(g_desc), 0);
TEST_HeaderPacketQoS(out_packet_pingreq, out_packet_pingreq(g_desc), 0);
TEST_HeaderPacketDup(out_packet_pingreq, out_packet_pingreq(g_desc), 0);
TEST_HeaderPacketRetain(out_packet_pingreq, out_packet_pingreq(g_desc), 0);


/* ========================================================================================= */
TEST_GROUP(out_packet_disconnect);
TEST_SETUP(out_packet_disconnect)
{
    g_desc = malloc(1024);
    memset(g_desc, 0xFF, 1024);
}

TEST_TEAR_DOWN(out_packet_disconnect)
{
    free(g_desc);
}

TEST_HeaderPacketType(out_packet_disconnect, out_packet_disconnect(g_desc), DISCONNECT);
TEST_HeaderPacketLength(out_packet_disconnect, out_packet_disconnect(g_desc), 0);
TEST_HeaderPacketQoS(out_packet_disconnect, out_packet_disconnect(g_desc), 0);
TEST_HeaderPacketDup(out_packet_disconnect, out_packet_disconnect(g_desc), 0);
TEST_HeaderPacketRetain(out_packet_disconnect, out_packet_disconnect(g_desc), 0);

/* ========================================================================================= */

TEST_GROUP(in_packet_connack);
static mqtt_frame_t *g_desc;
static mqtt_connack_opts_t g_ack;
TEST_SETUP(in_packet_connack)
{
    g_desc = malloc(1024);
    memset(g_desc, 0, 1024);
    g_desc->header.bits.type = CONNACK;
    g_desc->plen = 2;
}

TEST_TEAR_DOWN(in_packet_connack)
{
    free(g_desc);
}

TEST(in_packet_connack, InvalidType)
{
    g_desc->header.bits.type = CONNECT;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, in_packet_connack(g_desc, &g_ack));
}

TEST(in_packet_connack, InvalidPayloadLength)
{
    g_desc->plen = 4;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, in_packet_connack(g_desc, &g_ack));
}

TEST(in_packet_connack, InvalidReservedValues)
{
    uint8_t given[] = "\x10\x00";
    memcpy(g_desc->payload, given, 2);
    
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, in_packet_connack(g_desc, &g_ack));
}

TEST(in_packet_connack, SessionPresentAndConnected)
{
    uint8_t given[] = "\x01\x00";
    memcpy(g_desc->payload, given, 2);
    
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, in_packet_connack(g_desc, &g_ack));
    TEST_ASSERT_EQUAL(1, g_ack.session_present);
    TEST_ASSERT_EQUAL(0, g_ack.return_code);
}

TEST(in_packet_connack, SessionDoesNotPresent)
{
    uint8_t given[] = "\x00\x00";
    memcpy(g_desc->payload, given, 2);

    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, in_packet_connack(g_desc, &g_ack));
    TEST_ASSERT_EQUAL(0, g_ack.session_present);
}

TEST(in_packet_connack, ReturnCodeOk)
{
    uint8_t given[] = "\x00\x00";
    memcpy(g_desc->payload, given, 2);

    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, in_packet_connack(g_desc, &g_ack));
    TEST_ASSERT_EQUAL(0, g_ack.return_code);
}

TEST(in_packet_connack, ReturnCodeFailure)
{
    uint8_t given[] = "\x00\x05";
    memcpy(g_desc->payload, given, 2);

    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, in_packet_connack(g_desc, &g_ack));
    TEST_ASSERT_EQUAL(CONN_REFUSED_NOT_AUTHORIZED, g_ack.return_code);
}
/* ========================================================================================= */
TEST_GROUP(in_packet_publish);
static char g_in_topic[sizeof(g_topic)];
static char g_in_msg[sizeof(g_payload)];
TEST_SETUP(in_packet_publish)
{
    int marker = 0;
    g_msg = malloc(sizeof(mqtt_msg_t));
    g_msg->topic = g_in_topic;
    g_msg->msg = (uint8_t*)g_in_msg;
    g_desc = malloc(1024);
    memset(g_desc, 0, 1024);
    g_desc->header.bits.type = PUBLISH;
    g_desc->header.bits.qos = QoS1;
    /* Topic length */
    *(uint16_t*)&g_desc->payload[marker] = swap16(sizeof(g_topic));
    marker += 2;
    /* Topic name */
    strncpy((char*)&g_desc->payload[marker], g_topic, sizeof(g_topic));
    marker += sizeof(g_topic);
    /* packet id */
    *(uint16_t*)&g_desc->payload[marker] = swap16(0xabcd);
    marker += 2;
    /* message */
    strncpy((char*)&g_desc->payload[marker], g_payload, strlen(g_payload));
    marker += strlen(g_payload);
    g_desc->plen = marker;
}

TEST_TEAR_DOWN(in_packet_publish)
{
    free(g_desc);
    free(g_msg);
}

TEST_InInvalidHeaderPacketType(in_packet_publish, in_packet_publish(g_desc, g_msg));
TEST_InInvalidPayloadLength(in_packet_publish, in_packet_publish(g_desc, g_msg), 4);

TEST(in_packet_publish, PacketId)
{
    TEST_ASSERT_EQUAL_HEX16(0xabcd, in_packet_publish(g_desc, g_msg));
}

TEST(in_packet_publish, MsgQoS0)
{
    g_desc->header.bits.qos = QoS0;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, in_packet_publish(g_desc, g_msg));
    TEST_ASSERT_EQUAL(QoS0, g_msg->qos);
}

TEST(in_packet_publish, MsgQoS2)
{
    g_desc->header.bits.qos = QoS2;
    TEST_ASSERT_EQUAL(0xabcd, in_packet_publish(g_desc, g_msg));
    TEST_ASSERT_EQUAL(QoS2, g_msg->qos);
}

TEST(in_packet_publish, MsgRetain)
{
    g_desc->header.bits.retain = 1;
    TEST_ASSERT_EQUAL(0xabcd, in_packet_publish(g_desc, g_msg));
    TEST_ASSERT_EQUAL(1, g_msg->retain);
}

TEST(in_packet_publish, TopicName)
{
    TEST_ASSERT_EQUAL(0xabcd, in_packet_publish(g_desc, g_msg));
    TEST_ASSERT_EQUAL_STRING(g_topic, g_msg->topic);
}

TEST(in_packet_publish, Message)
{
    TEST_ASSERT_EQUAL(0xabcd, in_packet_publish(g_desc, g_msg));
    TEST_ASSERT_EQUAL(strlen(g_payload), g_msg->mlen);
    TEST_ASSERT_EQUAL_STRING_LEN(g_payload, g_msg->msg, strlen(g_payload));
}

TEST(in_packet_publish, DeclaredTopicLengthDoesntMatch)
{
    /* TODO: security check. Declared string length must match actual length */
    TEST_IGNORE_MESSAGE("Not implemented yet");
}


/* ========================================================================================= */
#define ACK_TYPE_TEST_GROUP(group, ptype) \
TEST_GROUP(group); \
TEST_SETUP(group) \
{ \
    g_desc = malloc(1024); \
    memset(g_desc, 0, 1024); \
    g_desc->header.bits.type = ptype; \
    g_desc->plen = 2; \
    *(uint16_t*)(g_desc->payload) = swap16(0xabcd); \
} \
 \
TEST_TEAR_DOWN(group) \
{ \
    free(g_desc); \
} \
TEST_InInvalidHeaderPacketType(group, group(g_desc)); \
TEST_InInvalidHeaderDup(group, group(g_desc)); \
TEST_InInvalidHeaderQoS(group, group(g_desc), QoS1); \
TEST_InInvalidHeaderRetain(group, group(g_desc)); \
TEST_InInvalidPayloadLength(group, group(g_desc), 1); \
 \
TEST(group, PacketId) \
{ \
    TEST_ASSERT_EQUAL(0xabcd, group(g_desc)); \
} 

ACK_TYPE_TEST_GROUP(in_packet_puback, PUBACK);
ACK_TYPE_TEST_GROUP(in_packet_pubrec, PUBREC);
ACK_TYPE_TEST_GROUP(in_packet_pubrel, PUBREL);
ACK_TYPE_TEST_GROUP(in_packet_pubcomp, PUBCOMP);
ACK_TYPE_TEST_GROUP(in_packet_unsuback, UNSUBACK);

/* ========================================================================================= */
TEST_GROUP(in_packet_pingresp);
TEST_SETUP(in_packet_pingresp)
{
    g_desc = malloc(1024);
    memset(g_desc, 0, 1024);
    g_desc->header.bits.type = PINGRESP;
    g_desc->plen = 0;
}

TEST_TEAR_DOWN(in_packet_pingresp)
{
    free(g_desc);
}
TEST_InInvalidHeaderPacketType(in_packet_pingresp, in_packet_pingresp(g_desc)); 
TEST_InInvalidHeaderDup(in_packet_pingresp, in_packet_pingresp(g_desc));
TEST_InInvalidHeaderQoS(in_packet_pingresp, in_packet_pingresp(g_desc), QoS1); 
TEST_InInvalidHeaderRetain(in_packet_pingresp, in_packet_pingresp(g_desc)); 
TEST_InInvalidPayloadLength(in_packet_pingresp, in_packet_pingresp(g_desc), 1); 

/* ========================================================================================= */
TEST_GROUP(in_packet_suback);
static suback_rc_t *g_subacks;
static uint32_t g_subacks_num;
TEST_SETUP(in_packet_suback)
{
    int i;

    g_desc = malloc(1024);
    memset(g_desc, 0, 1024);
    g_desc->header.bits.type = SUBACK;
    *(uint16_t*)g_desc->payload = swap16(0xabcd);
    for(i = 0; i < 100; i++)g_desc->payload[2 + i] = (i % 3);
    g_desc->plen = 102;

    g_subacks = malloc(100);
    g_subacks_num = 0;
}

TEST_TEAR_DOWN(in_packet_suback)
{
    free(g_desc);
    free(g_subacks);
}

TEST_InInvalidHeaderPacketType(in_packet_suback, in_packet_suback(g_desc, 0, NULL, &g_subacks_num)); 
TEST_InInvalidHeaderDup(in_packet_suback, in_packet_suback(g_desc, 0, NULL, &g_subacks_num));
TEST_InInvalidHeaderQoS(in_packet_suback, in_packet_suback(g_desc, 0, NULL, &g_subacks_num), QoS1); 
TEST_InInvalidHeaderRetain(in_packet_suback, in_packet_suback(g_desc, 0, NULL, &g_subacks_num)); 
TEST_InInvalidPayloadLength(in_packet_suback, in_packet_suback(g_desc, 0, NULL, &g_subacks_num), 3); 

TEST(in_packet_suback, PacketId) 
{ 
    TEST_ASSERT_EQUAL(0xabcd, in_packet_suback(g_desc, 100, g_subacks, &g_subacks_num)); 
}

TEST(in_packet_suback, IncomeMoreAckThanCanHandle)
{
    g_desc->plen = 2 + 11;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_NOT_ENOUGH_MEMORY, in_packet_suback(g_desc, 10, g_subacks, &g_subacks_num));
}

TEST(in_packet_suback, SubacksNumber)
{
    TEST_ASSERT_EQUAL(0xabcd, in_packet_suback(g_desc, 100, g_subacks, &g_subacks_num)); 
    TEST_ASSERT_EQUAL(100, g_subacks_num);
}

TEST(in_packet_suback, ReturnCodesQoS)
{
    int i;    

    TEST_ASSERT_EQUAL(0xabcd, in_packet_suback(g_desc, 100, g_subacks, &g_subacks_num)); 

    for(i = 0; i < 100; i++) {
        char msg[256];
        sprintf(msg, "ack mismatch. index = %d", i);
        TEST_ASSERT_EQUAL_MESSAGE(i%3, g_subacks[i], msg);
    }
}


TEST(in_packet_suback, InvalidReturnCode)
{
    g_desc->payload[99] = 0x06;

    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, 
            in_packet_suback(g_desc, 100, g_subacks, &g_subacks_num)); 
}

TEST(in_packet_suback, FailureReturnCode)
{
}

/* vim: set ts=4 sw=4 et sts */

