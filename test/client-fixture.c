#include "unity.h"
#include "unity_fixture.h"
#include "mqtt.h"
#include "frame-mock.h"
#include "packet-mock.h"
#include "system-mock.h"
#include <unistd.h>
#include <string.h>

static mqtt_client_t *g_client;
static mqtt_subs_opts_t *g_subs_opts;
static mqtt_msg_t *g_msg;
static mqtt_frame_t *g_desc;
static mqtt_connect_opts_t g_conn_opts = mqtt_connect_opts_defaults;
static bool g_sp;

static void expect_response_frame(mqtt_frame_t *frame, uint16_t packet_id, enum packet_type t)
{
    frame->header.bits.type = t;
    frame->header.bits.dup = 0;
    frame->header.bits.qos = 0;
    frame->header.bits.retain = 0;
    *(uint16_t*)frame->payload = swap16(packet_id);
    frame->plen = 2;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ReturnMemThruPtr_desc(frame, 16);
}

static void expect_pingresp_frame(mqtt_frame_t *frame)
{
    frame->header.byte = 0;
    frame->header.bits.type = PINGRESP;
    frame->plen = 0;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ReturnMemThruPtr_desc(frame, 16);
    in_packet_pingresp_ExpectAndReturn(g_client->buffer.desc, MQTT_ERRNO_OK);
}

static void expect_publish_frame(mqtt_frame_t *frame)
{
    static const char topic[] = "topic/example";
    static const char payload[] = "example payload";
    int marker = 0;
 
    frame->header.bits.type = PUBLISH;
    *(uint16_t*)&frame->payload[marker] = swap16(sizeof(topic));
    marker += 2;
    strncpy((char*)&frame->payload[marker], topic, sizeof(topic));
    marker += sizeof(topic);
    *(uint16_t*)&frame->payload[marker] = swap16(0xabcd);
    marker += 2;
    strncpy((char*)&frame->payload[marker], payload, strlen(payload));
    marker += strlen(payload);
    frame->plen = marker;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ReturnMemThruPtr_desc(frame, marker + 2);
    in_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
}

static void expect_suback_frame(mqtt_frame_t *frame) 
{
    frame->header.bits.type = SUBACK;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ReturnMemThruPtr_desc(frame, 1);
}

static void mocks_SetUp()
{
    system_mock_Init();
    frame_mock_Init();
    packet_mock_Init();

}

static void mocks_TearDown()
{
    system_mock_Verify();
    frame_mock_Verify();
    packet_mock_Verify();

    system_mock_Destroy();
    frame_mock_Destroy();
    packet_mock_Destroy();
}


TEST_GROUP(mqtt_connect);

TEST_SETUP(mqtt_connect)
{
    mocks_SetUp();
    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.size = 1024;
    g_client->buffer.desc = calloc(1, g_client->buffer.size);
    g_sp = false;
}

TEST_TEAR_DOWN(mqtt_connect)
{
    free(g_client->buffer.desc);
    free(g_client);
    mocks_TearDown();
}

TEST(mqtt_connect, InvalidConnectOptions)
{
    out_packet_connect_ExpectAndReturn(&g_conn_opts, g_client->buffer.desc, MQTT_ERRNO_BUFFER_OVF);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BUFFER_OVF, mqtt_connect(g_client, &g_conn_opts, &g_sp));
}

TEST(mqtt_connect, SendingFrameFailed)
{
    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 0, MQTT_ERRNO_MALFORMED_PACKET);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_connect(g_client, &g_conn_opts, &g_sp));
}

TEST(mqtt_connect, ReceiveFrameError)
{
    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_MALFORMED_PACKET);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_connect(g_client, &g_conn_opts, &g_sp));
}

TEST(mqtt_connect, ReceivedInvalidConnackOrUnexpectedMessage)
{
    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_MALFORMED_PACKET);
    in_packet_connack_IgnoreArg_opts();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_connect(g_client, &g_conn_opts, &g_sp));
}

TEST(mqtt_connect, CheckSessionPresentReceivedCorrectly)
{
    mqtt_connack_opts_t opts;
    
    opts.return_code = CONN_ACCEPTED;
    opts.session_present = true;

    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_OK);
    in_packet_connack_IgnoreArg_opts();
    in_packet_connack_ReturnMemThruPtr_opts(&opts, sizeof(mqtt_connack_opts_t));
    mqtt_clock_IgnoreAndReturn(0);

    mqtt_connect(g_client, &g_conn_opts, &g_sp);
    TEST_ASSERT_EQUAL(true, g_sp);
}

TEST(mqtt_connect, IgnoreSessionPresentReturnIfNullPointerProvided)
{
    mqtt_connack_opts_t opts;
    
    opts.return_code = CONN_ACCEPTED;
    opts.session_present = true;

    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_OK);
    in_packet_connack_IgnoreArg_opts();
    in_packet_connack_ReturnMemThruPtr_opts(&opts, sizeof(mqtt_connack_opts_t));
    mqtt_clock_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL(CONN_ACCEPTED, mqtt_connect(g_client, &g_conn_opts, NULL));
}

TEST(mqtt_connect, ConnectedSuccessfully)
{
    mqtt_connack_opts_t opts;
    
    opts.return_code = CONN_ACCEPTED;
    opts.session_present = true;

    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_OK);
    in_packet_connack_IgnoreArg_opts();
    in_packet_connack_ReturnMemThruPtr_opts(&opts, sizeof(mqtt_connack_opts_t));
    mqtt_clock_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL(CONN_ACCEPTED, mqtt_connect(g_client, &g_conn_opts, &g_sp));
}

TEST(mqtt_connect, BrokerRefusedConnection)
{
    mqtt_connack_opts_t opts;
    
    opts.return_code = CONN_REFUSED_NOT_AUTHORIZED;
    opts.session_present = true;

    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_OK);
    in_packet_connack_IgnoreArg_opts();
    in_packet_connack_ReturnMemThruPtr_opts(&opts, sizeof(mqtt_connack_opts_t));
    mqtt_clock_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL(CONN_REFUSED_NOT_AUTHORIZED, mqtt_connect(g_client, &g_conn_opts, &g_sp));
}

TEST(mqtt_connect, CheckIfSessionDataInitializedCorrectly)
{
    mqtt_connack_opts_t opts;
    opts.return_code = CONN_ACCEPTED;
    opts.session_present = true;

    g_conn_opts.keep_alive = 65300;

    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_OK);
    in_packet_connack_IgnoreArg_opts();
    in_packet_connack_ReturnMemThruPtr_opts(&opts, sizeof(mqtt_connack_opts_t));
    mqtt_clock_ExpectAndReturn(1234);

    TEST_ASSERT_EQUAL(CONN_ACCEPTED, mqtt_connect(g_client, &g_conn_opts, &g_sp));
    TEST_ASSERT_EQUAL_MESSAGE(65300, g_client->session.keep_alive, "keep_alive");
    TEST_ASSERT_EQUAL_MESSAGE(1, g_client->session.packet_id, "packet_id");
    TEST_ASSERT_EQUAL_MESSAGE(1234, g_client->session.last_sent, "last_sent");
}

TEST(mqtt_connect, CheckCorrectDeadlinePassedToFunctions)
{
    mqtt_connack_opts_t opts;

    g_client->timeout = 30;
    
    opts.return_code = CONN_ACCEPTED;
    opts.session_present = true;

    out_packet_connect_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, (1234 + 30), MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, (1234 + 30), MQTT_ERRNO_OK);
    in_packet_connack_ExpectAndReturn(g_client->buffer.desc, NULL, MQTT_ERRNO_OK);
    in_packet_connack_IgnoreArg_opts();
    in_packet_connack_ReturnMemThruPtr_opts(&opts, sizeof(mqtt_connack_opts_t));
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_clock_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL(CONN_ACCEPTED, mqtt_connect(g_client, &g_conn_opts, &g_sp));

}

/* ========================================================================================= */
TEST_GROUP(mqtt_disconnect);
TEST_SETUP(mqtt_disconnect)
{
    mocks_SetUp();
    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->session.packet_id = 0x1234;
    g_sp = false;
}

TEST_TEAR_DOWN(mqtt_disconnect)
{
    free(g_client->buffer.desc);
    free(g_client);
    mocks_TearDown();
}

TEST(mqtt_disconnect, SendingFrameFailed)
{
    out_packet_disconnect_ExpectAndReturn(g_client->buffer.desc, MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 0, MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_disconnect(g_client));
}

TEST(mqtt_disconnect, PassDeadlineValueAndExpectTimeout)
{
    g_client->timeout = 30;
    mqtt_clock_ExpectAndReturn(0);
    out_packet_disconnect_ExpectAndReturn(g_client->buffer.desc, MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 30, MQTT_ERRNO_TIMEOUT);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_TIMEOUT, mqtt_disconnect(g_client));
}
/* ========================================================================================= */
TEST_GROUP(mqtt_publish_qos0);
TEST_SETUP(mqtt_publish_qos0)
{
    mocks_SetUp();

    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->session.packet_id = 0x1234;
    g_client->session.last_sent = 0xabcd;

    g_msg = calloc(1, 512);
    g_msg->topic = "topic/test";
    g_msg->msg = (uint8_t*)"example payload";
    g_msg->mlen = strlen((char*)g_msg->msg);
}

TEST_TEAR_DOWN(mqtt_publish_qos0)
{
    free(g_client->buffer.desc);
    free(g_client);
    free(g_msg);
    mocks_TearDown();
}

TEST(mqtt_publish_qos0, InvalidPublishData)
{
    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_MALFORMED_PACKET);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish_qos0, SendingPublishFrameFailed)
{
    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 0, MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish_qos0, CheckIfPacketIdIncremented)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;

    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 0, MQTT_ERRNO_OK);
    mqtt_clock_IgnoreAndReturn(0);
    
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_publish(g_client, g_msg));
    TEST_ASSERT_EQUAL(expected_packet_id, g_client->session.packet_id);

    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_MALFORMED_PACKET);
    mqtt_publish(g_client, g_msg);
    TEST_ASSERT_EQUAL(expected_packet_id + 1, g_client->session.packet_id);
}

TEST(mqtt_publish_qos0, CheckIfLastSentTimeUpdated)
{
    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(1234);

    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_publish(g_client, g_msg));
    TEST_ASSERT_EQUAL(1234, g_client->session.last_sent);
}

TEST(mqtt_publish_qos0, CheckCorrectDeadlinePassedToSendFunction)
{
    g_client->timeout = 30;

    mqtt_clock_ExpectAndReturn(0);
    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 30, MQTT_ERRNO_OK);
    mqtt_clock_IgnoreAndReturn(0);
    mqtt_publish(g_client, g_msg);
}

/* ========================================================================================= */
TEST_GROUP(mqtt_publish);

TEST_SETUP(mqtt_publish)
{
    mocks_SetUp();
    
    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->session.packet_id = 0x1234;
    g_client->session.last_sent = 10;
    g_client->session.keep_alive = 60;
    g_sp = false;

    g_msg = calloc(1, 512);
    g_msg->topic = "topic/test";
    g_msg->qos = QoS1;
    g_msg->msg = (uint8_t*)"example payload";
    g_msg->mlen = strlen((char*)g_msg->msg);

    g_desc = calloc(1, 1024);
    
    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, 0, MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
}

TEST_TEAR_DOWN(mqtt_publish)
{
    free(g_client->buffer.desc);
    free(g_client);
    free(g_msg);
    free(g_desc);
    mocks_TearDown();
}

TEST(mqtt_publish, ReceiveError)
{
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, ReceivedMalformedPacket)
{
    expect_response_frame(g_desc, 0, PUBACK);
    in_packet_pub_response_IgnoreAndReturn(MQTT_ERRNO_MALFORMED_PACKET);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, HandlePublishRequestDuringSession)
{
    uint8_t puback[16];
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    mqtt_frame_t* publish = g_desc;

    expect_publish_frame(publish);
    mqtt_clock_ExpectAndReturn(20);
    expect_response_frame((mqtt_frame_t*)puback, expected_packet_id, PUBACK);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);

    mqtt_clock_IgnoreAndReturn(0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, HandlePingrespDuringSession)
{
    uint8_t pingresp[16];
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    mqtt_frame_t* puback = g_desc;
    g_client->session.pending.ping = true;

    expect_pingresp_frame((mqtt_frame_t*)pingresp);
    expect_response_frame(puback, expected_packet_id, PUBACK);
    in_packet_pub_response_IgnoreAndReturn(MQTT_ERRNO_MALFORMED_PACKET);
    mqtt_clock_ExpectAndReturn(30); /* update last_sent to this value */
    mqtt_clock_ExpectAndReturn(70); /* do not send another keep alive packet */
    mqtt_publish(g_client, g_msg);
    TEST_ASSERT_EQUAL(false, g_client->session.pending.ping);
    TEST_ASSERT_EQUAL(30, g_client->session.last_sent);
}

TEST(mqtt_publish, FailBecauseReceivedUnexpectedPacket)
{
    expect_response_frame(g_desc, 0, SUBSCRIBE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_TYPE, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, FailBecauseReceivedInvalidPacketId)
{
    expect_response_frame(g_desc, 123, PUBACK);
    in_packet_pub_response_IgnoreAndReturn(321);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_ID, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, PubackReceivedSuccessfully)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    expect_response_frame(g_desc, expected_packet_id, PUBACK);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    mqtt_clock_IgnoreAndReturn(0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, PubcompReceivedSuccessfully)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    expect_response_frame(g_desc, expected_packet_id, PUBCOMP);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    mqtt_clock_IgnoreAndReturn(0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_publish(g_client, g_msg));
}

TEST(mqtt_publish, CheckPubrelSentAfterPubrec)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    expect_response_frame(g_desc, expected_packet_id, PUBREC);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    out_packet_pubrel_ExpectAndReturn(expected_packet_id, g_client->buffer.desc, MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_BROKEN_PIPE);
    mqtt_publish(g_client, g_msg);
}

TEST(mqtt_publish, CheckPacketIdIncrementedIfError)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_BROKEN_PIPE);
    mqtt_publish(g_client, g_msg);
    TEST_ASSERT_EQUAL(expected_packet_id, g_client->session.packet_id);
}

TEST(mqtt_publish, CheckPacketIdIncrementedIfSuccess)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    expect_response_frame(g_desc, expected_packet_id, PUBCOMP);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    mqtt_clock_IgnoreAndReturn(0);
    mqtt_publish(g_client, g_msg);
    TEST_ASSERT_EQUAL(expected_packet_id, g_client->session.packet_id);
}

TEST(mqtt_publish, CheckIfLastSentTimeUpdatedIfSuccess)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    expect_response_frame(g_desc, expected_packet_id, PUBCOMP);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_publish(g_client, g_msg);
    TEST_ASSERT_EQUAL(1234, g_client->session.last_sent);
}

TEST(mqtt_publish, CheckIfLastSentTimeNotUpdatedIfError)
{
    uint32_t expected_last_sent = g_client->session.last_sent;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, 0, MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_NOT_EQUAL(MQTT_ERRNO_OK, mqtt_publish(g_client, g_msg));
    TEST_ASSERT_EQUAL(expected_last_sent, g_client->session.last_sent);
}

TEST(mqtt_publish, CheciIfPendingOPublishCleared)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    expect_response_frame(g_desc, expected_packet_id, PUBCOMP);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    mqtt_clock_IgnoreAndReturn(0);
    mqtt_publish(g_client, g_msg);
    TEST_ASSERT_EQUAL(false, g_client->session.pending.opublish);
}

/* ========================================================================================= */
static char *g_subs_example_topics[] = {
    "root/topic/1",
    "root/topic/2",
    "root/topic/3",
    "root/topic/4",
    "root/topic/5",
    "root/topic/6",
    "root/topic/7",
    "root/topic/8",
    "root/topic/9",
    "root/topic/10",
};
static suback_rc_t g_expected_codes[10];

TEST_GROUP(mqtt_subscribe);
TEST_SETUP(mqtt_subscribe)
{
    int i;
    mocks_SetUp();

    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->session.packet_id = 0x1234;
    g_client->session.last_sent = 10;
    g_client->session.keep_alive = 60;

    g_subs_opts = calloc(1, sizeof(mqtt_subs_opts_t)*10);
    for(i = 0; i < 10; i++) {
        g_subs_opts[i].topic = g_subs_example_topics[i];
        g_subs_opts[i].qos = QoS1;
        g_expected_codes[i] = QoS1;
    }
    g_desc = calloc(1, 1024);
}

TEST_TEAR_DOWN(mqtt_subscribe)
{
    free(g_desc);
    free(g_client->buffer.desc);
    free(g_client);
    free(g_subs_opts);
    mocks_TearDown();
}

TEST(mqtt_subscribe, InvalidSubscribeOpts)
{
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_INVALID_VALUE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_INVALID_VALUE, mqtt_subscribe(g_client, g_subs_opts, 1));
}

TEST(mqtt_subscribe, FailBecauseBufferTooSmallToFitTopics)
{
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_BUFFER_OVF);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BUFFER_OVF, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, SendingSubscribeFrameFailed)
{
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, ReceiveError)
{
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    mqtt_frame_deserialize_IgnoreAndReturn(MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, ReceivedMalformedPacket)
{
    g_desc->header.bits.type = SUBACK;

    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_IgnoreAndReturn(MQTT_ERRNO_MALFORMED_PACKET);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, HandlePublishRequestDuringSession)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    uint32_t returned = 0;
    uint8_t publish[1024];

    memset(publish, 0, sizeof(publish));
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_publish_frame((mqtt_frame_t*)publish); /* QoS0 */
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_ExpectAndReturn(NULL, 0, NULL, 0, expected_packet_id);
    in_packet_suback_IgnoreArg_desc();
    in_packet_suback_IgnoreArg_max();
    in_packet_suback_IgnoreArg_rcodes();
    in_packet_suback_IgnoreArg_num();
    in_packet_suback_ReturnThruPtr_num(&returned);
    mqtt_clock_ExpectAndReturn(0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_subscribe(g_client, g_subs_opts, 0));
}

TEST(mqtt_subscribe, HandlePingrespDuringSession)
{
    uint8_t pingresp[16];
    g_client->session.pending.ping = true;

    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    expect_pingresp_frame((mqtt_frame_t*)pingresp);
    expect_suback_frame(g_desc);
    in_packet_suback_IgnoreAndReturn(MQTT_ERRNO_MALFORMED_PACKET);
    mqtt_clock_ExpectAndReturn(20); 
    mqtt_clock_ExpectAndReturn(30); /* update last_sent to this value */
    mqtt_clock_ExpectAndReturn(70); /* do not send another keep alive packet */

    mqtt_subscribe(g_client, g_subs_opts, 0);
    TEST_ASSERT_EQUAL(false, g_client->session.pending.ping);
    TEST_ASSERT_EQUAL(30, g_client->session.last_sent);
}

TEST(mqtt_subscribe, FailBecauseReceivedUnexpectedPacket)
{
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_response_frame(g_desc, 0, SUBSCRIBE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_TYPE, mqtt_subscribe(g_client, g_subs_opts, 0));
}

TEST(mqtt_subscribe, FailBecauseReceivedInvalidPacketId)
{
    g_desc->header.bits.type = SUBACK;

    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_IgnoreAndReturn(g_client->session.packet_id - 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_ID, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, SubackLessReturnCodesCountThanExpected)
{
    uint32_t returned = 8;
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    g_desc->header.bits.type = SUBACK;
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_ExpectAndReturn(NULL, 0, NULL, 0, expected_packet_id);
    in_packet_suback_IgnoreArg_desc();
    in_packet_suback_IgnoreArg_max();
    in_packet_suback_IgnoreArg_rcodes();
    in_packet_suback_IgnoreArg_num();
    in_packet_suback_ReturnThruPtr_num(&returned);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_TOO_FEW_ACKS, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, SubackMoreReturnCodesCountThanExpected)
{
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_IgnoreAndReturn(MQTT_ERRNO_NOT_ENOUGH_MEMORY);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_NOT_ENOUGH_MEMORY, mqtt_subscribe(g_client, g_subs_opts, 10));

}
TEST(mqtt_subscribe, SubackSingleFailure)
{
    uint32_t returned = 10;
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    g_expected_codes[3] = SUBS_FAILED;
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_ExpectAndReturn(NULL, 0, NULL, 0, expected_packet_id);
    in_packet_suback_IgnoreArg_desc();
    in_packet_suback_IgnoreArg_max();
    in_packet_suback_IgnoreArg_rcodes();
    in_packet_suback_IgnoreArg_num();
    in_packet_suback_ReturnMemThruPtr_rcodes(g_expected_codes, sizeof(g_expected_codes));
    in_packet_suback_ReturnThruPtr_num(&returned);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_SUBSCRIBE_FAILED, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, SubackSubWithWrongQoS)
{
    uint32_t returned = 10;
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    g_expected_codes[5] = QoS2;
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_ExpectAndReturn(NULL, 0, NULL, 0, expected_packet_id);
    in_packet_suback_IgnoreArg_desc();
    in_packet_suback_IgnoreArg_max();
    in_packet_suback_IgnoreArg_rcodes();
    in_packet_suback_IgnoreArg_num();
    in_packet_suback_ReturnMemThruPtr_rcodes(g_expected_codes, sizeof(g_expected_codes));
    in_packet_suback_ReturnThruPtr_num(&returned);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_SUBS_WRONG_QOS, mqtt_subscribe(g_client, g_subs_opts, 10));
}

TEST(mqtt_subscribe, LastSentTimeUpdated)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    uint32_t returned = 10;
    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_clock_ExpectAndReturn(20);
    expect_suback_frame(g_desc);
    in_packet_suback_ExpectAndReturn(NULL, 0, NULL, 0, expected_packet_id);
    in_packet_suback_IgnoreArg_desc();
    in_packet_suback_IgnoreArg_max();
    in_packet_suback_IgnoreArg_rcodes();
    in_packet_suback_IgnoreArg_num();
    in_packet_suback_ReturnMemThruPtr_rcodes(g_expected_codes, sizeof(g_expected_codes));
    in_packet_suback_ReturnThruPtr_num(&returned);
    mqtt_clock_ExpectAndReturn(0x1234);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_subscribe(g_client, g_subs_opts, 10));
    TEST_ASSERT_EQUAL(0x1234, g_client->session.last_sent);
}

/* ========================================================================================= */
TEST_GROUP(remote_host_publish);
TEST_SETUP(remote_host_publish)
{
    mocks_SetUp();

    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->session.packet_id = 0x1234;
    g_desc = calloc(1, 1024);
}

TEST_TEAR_DOWN(remote_host_publish)
{
    free(g_client->buffer.desc);
    free(g_client);
    free(g_desc);
    mocks_TearDown();
}

/* ========================================================================================= */
TEST_GROUP(keep_alive);
TEST_SETUP(keep_alive)
{
    mocks_SetUp();

    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->session.packet_id = 0x1234;
    g_desc = calloc(1, 1024);
}

TEST_TEAR_DOWN(keep_alive)
{
    free(g_client->buffer.desc);
    free(g_client);
    free(g_desc);
    mocks_TearDown();
}

/* ========================================================================================= */
TEST_GROUP(check_deadlines);
TEST_SETUP(check_deadlines)
{
    mocks_SetUp();

    g_client = calloc(1, sizeof(mqtt_client_t));
    g_client->buffer.desc = calloc(1, 1024);
    g_client->buffer.size = 1024;
    g_client->timeout = 30;
    g_client->session.packet_id = 0x1234;
    g_client->session.last_sent = 1234;
    g_client->session.keep_alive = 60;
    g_desc = calloc(1, 1024);
    
    g_msg = calloc(1, 512);
    g_msg->topic = "topic/test";
    g_msg->qos = QoS1;
    g_msg->msg = (uint8_t*)"example payload";
    g_msg->mlen = strlen((char*)g_msg->msg);

}

TEST_TEAR_DOWN(check_deadlines)
{
    free(g_client->buffer.desc);
    free(g_client);
    free(g_desc);
    free(g_msg);
    mocks_TearDown();
}

TEST(check_deadlines, mqtt_publish)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    
    out_packet_publish_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, (1234 + 30), MQTT_ERRNO_OK);

    g_desc->header.bits.type = PUBACK;
    g_desc->header.bits.dup = 0;
    g_desc->header.bits.qos = 0;
    g_desc->header.bits.retain = 0;
    *(uint16_t*)g_desc->payload = swap16(expected_packet_id);
    g_desc->plen = 2;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, (1234 + 30), MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ReturnMemThruPtr_desc(g_desc, 16);
    in_packet_pub_response_IgnoreAndReturn(expected_packet_id);
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_publish(g_client, g_msg);
}

TEST(check_deadlines, mqtt_subscribe)
{
    uint16_t expected_packet_id = g_client->session.packet_id + 1;
    uint32_t returned = 0;

    out_packet_subscribe_IgnoreAndReturn(MQTT_ERRNO_OK);
    mqtt_frame_serialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, (1234 + 30), MQTT_ERRNO_OK);
    
    g_desc->header.bits.type = SUBACK;
    mqtt_frame_deserialize_ExpectAndReturn(g_client->fd, g_client->buffer.desc, g_client->buffer.size, (1234 + 30), MQTT_ERRNO_OK);
    mqtt_frame_deserialize_ReturnMemThruPtr_desc(g_desc, 1);
    in_packet_suback_ExpectAndReturn(NULL, 0, NULL, 0, expected_packet_id);
    in_packet_suback_IgnoreArg_desc();
    in_packet_suback_IgnoreArg_max();
    in_packet_suback_IgnoreArg_rcodes();
    in_packet_suback_IgnoreArg_num();
    in_packet_suback_ReturnThruPtr_num(&returned);
    
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_clock_ExpectAndReturn(1234);
    mqtt_clock_ExpectAndReturn(1235);
    mqtt_subscribe(g_client, g_subs_opts, 0);
}

