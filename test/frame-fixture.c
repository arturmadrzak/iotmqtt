#include "unity.h"
#include "unity_fixture.h"
#include "mqtt.h"
#include "system-mock.h"
#include <unistd.h>
#include <string.h>

static mqtt_frame_t* g_desc;
static uint32_t g_desc_len;
static uint8_t g_expected_frame[] = "\x10\x0CMQTT_PAYLOAD";

TEST_GROUP(mqtt_frame_serialize);

TEST_SETUP(mqtt_frame_serialize)
{
    system_mock_Init();
    g_desc_len = 1024;
    g_desc = calloc(1, g_desc_len);
}

TEST_TEAR_DOWN(mqtt_frame_serialize)
{
    free(g_desc);
}

TEST(mqtt_frame_serialize, FailBecauseInvalidControlPacketType)
{
    g_desc->header.bits.type = 0;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_TYPE, mqtt_frame_serialize(NULL, g_desc, 0));
    g_desc->header.bits.type = 15;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_TYPE, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfControlPacketTypeSetCorrectly)
{
    uint8_t expected[] = { 0x88, 0x00 };
    g_desc->header.bits.type = 8;
    g_desc->header.bits.dup = 1; 
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfDuplicateFlagIsSet)
{
    uint8_t expected[] = { 0x18, 0x00 };
    g_desc->header.bits.type = 1;
    g_desc->header.bits.dup = 1; 
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfQoSFlagsAreNotSetOnQoS0)
{
    uint8_t expected[] = { 0x10, 0x00 };
    g_desc->header.bits.type = 1;
    g_desc->header.bits.qos = QoS0;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfQoSFlagsAreSetOnQoS1)
{
    uint8_t expected[] = { 0x12, 0x00 };
    g_desc->header.bits.type = 1;
    g_desc->header.bits.qos = QoS1;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfQoSFlagsAreSetOnQoS2)
{
    uint8_t expected[] = { 0x14, 0x00 };
    g_desc->header.bits.type = 1;
    g_desc->header.bits.qos = QoS2;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, FailBecauseNotAllowedQoS)
{
    g_desc->header.bits.type = 1;
    g_desc->header.bits.qos = 3;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfRetainFlagIsSet)
{
    uint8_t expected[] = { 0x11, 0x00 };
    g_desc->header.bits.type = 1;
    g_desc->header.bits.retain = 1;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, RemainingLengthEncodedOnOneByte)
{
    uint8_t expected[] = { 0x10, 0x7F };
    g_desc->header.bits.type = 1;
    g_desc->plen = 0x7F;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 0x7F, 0, 0x7F);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, RemainingLengthEncodedOnTwoBytes)
{
    uint8_t expected[] = { 0x10, 0xC1, 0x02 };
    g_desc->header.bits.type = 1;
    g_desc->plen = 321;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 2, 0, 2);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 321, 0, 321);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, RemainingLengthEncodedOnTwoThree)
{
    uint8_t expected[] = { 0x10, 0xD4, 0xA0, 0x01 };
    g_desc->header.bits.type = 1;
    g_desc->plen = 20564;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 3, 0, 3);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 20564, 0, 20564);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, RemainingLengthEncodedOnTwoFour)
{
    uint8_t expected[] = { 0x10, 0xBF, 0xFE, 0x82, 0x04 };
    g_desc->header.bits.type = 1;
    g_desc->plen = 8437567;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 4, 0, 4);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 8437567, 0, 8437567);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, InvalidRemainingLengthEncodedOnTwoFour)
{
    g_desc->header.bits.type = 1;
    g_desc->plen = 268435456;
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, CheckIfPayloadSendCorrectly)
{
    uint8_t expected[] = "\x10\x0CMQTT_PAYLOAD";
    g_desc->header.bits.type = 1;
    g_desc->plen = sprintf((char*)g_desc->payload, "MQTT_PAYLOAD");
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, g_desc->plen, 0, g_desc->plen);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, FailBecauseCantSendHeader)
{
    g_desc->header.bits.type = 1;
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 1, 0, MQTT_ERRNO_BROKEN_PIPE);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, FailBecauseCantSendRemainingLength)
{
    uint8_t expected[] = "\x10\x01";
    g_desc->header.bits.type = 1;
    g_desc->plen = 1;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, MQTT_ERRNO_BROKEN_PIPE);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_serialize(NULL, g_desc, 0));
}

TEST(mqtt_frame_serialize, FailBecauseCantSendPayload)
{     
    uint8_t expected[] = "\x10\x01";
    g_desc->header.bits.type = 1;
    g_desc->plen = 1;
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, 0, 1);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 1, 0, MQTT_ERRNO_BROKEN_PIPE);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_serialize(NULL, g_desc, 0));
}

/* ========================================================================================= */

TEST_GROUP(mqtt_frame_serialize_timeout);

TEST_SETUP(mqtt_frame_serialize_timeout)
{
    system_mock_Init();
    g_desc_len = 1024;
    g_desc = calloc(1, g_desc_len);
    g_desc->header.bits.type = 1;
    g_desc->plen = sprintf((char*)g_desc->payload, "MQTT_PAYLOAD");
}

TEST_TEAR_DOWN(mqtt_frame_serialize_timeout)
{
    free(g_desc);
}

TEST(mqtt_frame_serialize_timeout, TimeoutBecauseDeadlineReached)
{
    mqtt_clock_ExpectAndReturn(10);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_TIMEOUT, mqtt_frame_serialize(NULL, g_desc, 10));
}

TEST(mqtt_frame_serialize_timeout, BrokenPipeCausedByTimeout)
{
    mqtt_clock_ExpectAndReturn(8);
    mqtt_io_write_ExpectAndReturn(NULL, &g_expected_frame[0], 1, (10 - 8), 1);
    mqtt_clock_ExpectAndReturn(8);
    mqtt_io_write_ExpectAndReturn(NULL, &g_expected_frame[1], 1, (10 - 8), 1);
    mqtt_clock_ExpectAndReturn(10);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_serialize(NULL, g_desc, 10));
}

TEST(mqtt_frame_serialize_timeout, InTime)
{    
    uint8_t expected[] = "\x10\x0CMQTT_PAYLOAD";
    g_desc->header.bits.type = 1;
    g_desc->plen = sprintf((char*)g_desc->payload, "MQTT_PAYLOAD");
    mqtt_clock_ExpectAndReturn(3);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[0], 1, (10 - 3), 1);
    mqtt_clock_ExpectAndReturn(5);
    mqtt_io_write_ExpectAndReturn(NULL, &expected[1], 1, (10 - 5), 1);
    mqtt_clock_ExpectAndReturn(8);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, g_desc->plen, (10 - 8), g_desc->plen);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_serialize(NULL, g_desc, 10));
}

TEST(mqtt_frame_serialize_timeout, NetworkTimeout)
{
    mqtt_clock_ExpectAndReturn(8);
    mqtt_io_write_ExpectAndReturn(NULL, NULL, 1, (10 - 8), MQTT_ERRNO_TIMEOUT);
    mqtt_io_write_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_TIMEOUT, mqtt_frame_serialize(NULL, g_desc, 10));
}

/* ========================================================================================= */
static void expect_chunk(uint8_t* value, int len, uint32_t deadline)
{
    mqtt_io_read_ExpectAndReturn(NULL, NULL, len, deadline, len);
    mqtt_io_read_ReturnMemThruPtr_buffer(value, len);
    mqtt_io_read_IgnoreArg_buffer();
}

static void expect_hdr(uint8_t *h, uint32_t deadline)
{
    expect_chunk(h, 1, deadline);
}

static void expect_remlen(uint8_t* value, int len, uint32_t deadline)
{
    int i;
    for(i = 0; i < len; i++)
        expect_chunk(&value[i], 1, deadline);
}

static void expect_payload(uint8_t* value, int len, uint32_t deadline)
{
    expect_chunk(value, len, deadline);
}

static void ignore_payload(int len)
{
    mqtt_io_read_ExpectAndReturn(NULL, NULL, len, 0, len);
    mqtt_io_read_IgnoreArg_buffer();
}

TEST_GROUP(mqtt_frame_deserialize);

TEST_SETUP(mqtt_frame_deserialize)
{
    system_mock_Init();
    g_desc_len = 1024;
    g_desc = calloc(1, g_desc_len);
}

TEST_TEAR_DOWN(mqtt_frame_deserialize)
{
    free(g_desc);
}

TEST(mqtt_frame_deserialize, FailBecauseInvalidControlPacketType)
{
    uint8_t given[] = "\x00";
    expect_hdr(&given[0], 0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_PACKET_TYPE, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
}

TEST(mqtt_frame_deserialize, CheckIfControlPacketTypeSetCorrectly)
{
    uint8_t given[] = "\x10\x00";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0);
    TEST_ASSERT_EQUAL(1, g_desc->header.bits.type);
}

TEST(mqtt_frame_deserialize, CheckIfDuplicateFlagIsSet)
{
    uint8_t given[] = "\x18\x00";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0);
    TEST_ASSERT_EQUAL(1, g_desc->header.bits.dup);
}

TEST(mqtt_frame_deserialize, CheckIfQoSFlagsAreNotSetOnQoS0)
{
    uint8_t given[] = "\x10\x00";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0);
    TEST_ASSERT_EQUAL(QoS0, g_desc->header.bits.qos);
}

TEST(mqtt_frame_deserialize, CheckIfQoSFlagsAreSetOnQoS1)
{
    uint8_t given[] = "\x12\x00";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0);
    TEST_ASSERT_EQUAL(QoS1, g_desc->header.bits.qos);
}

TEST(mqtt_frame_deserialize, CheckIfQoSFlagsAreSetOnQoS2)
{    
    uint8_t given[] = "\x14\x00";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0);
    TEST_ASSERT_EQUAL(QoS2, g_desc->header.bits.qos);
}

TEST(mqtt_frame_deserialize, FailBecauseNotAllowedQoS)
{
    uint8_t given[] = "\x16\x00";
    expect_hdr(&given[0], 0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_WRONG_QOS, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
}

TEST(mqtt_frame_deserialize, CheckIfRetainFlagIsSet)
{
    uint8_t given[] = "\x11\x00";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0);
    TEST_ASSERT_EQUAL(1, g_desc->header.bits.retain);
}


TEST(mqtt_frame_deserialize, RemainingLengthEncodedOnOneByte)
{
    uint8_t given[] = "\x10\x01M";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    ignore_payload(1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
    TEST_ASSERT_EQUAL(1, g_desc->plen);
}

TEST(mqtt_frame_deserialize, RemainingLengthEncodedOnTwoBytes)
{
    uint8_t given[] = "\x10\xC1\x02";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 2, 0);
    ignore_payload(321);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
    TEST_ASSERT_EQUAL(321, g_desc->plen);
}

TEST(mqtt_frame_deserialize, RemainingLengthEncodedOnTwoThree)
{
    uint8_t given[] = "\x10\xD4\xA0\x01";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 3, 0);
    ignore_payload(20564);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, 20564 + sizeof(mqtt_frame_t) , 0));
    TEST_ASSERT_EQUAL(20564, g_desc->plen);
}

TEST(mqtt_frame_deserialize, RemainingLengthEncodedOnTwoFour)
{
    uint8_t given[] = "\x10\xBF\xFE\x82\x04";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 4, 0);
    ignore_payload(8437567);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, 8437567 + sizeof(mqtt_frame_t), 0));
    TEST_ASSERT_EQUAL(8437567, g_desc->plen);
}

TEST(mqtt_frame_deserialize, CheckRemainingLengthUpdatedIfFrameDescNotZero)
{
    uint8_t given[] = "\x10\x01\xaa";
    g_desc->plen = 0x123456;
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 2, 0);
    ignore_payload(1);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
    TEST_ASSERT_EQUAL(1, g_desc->plen);
}

TEST(mqtt_frame_deserialize, InvalidRemainingLengthEncodedOnTwoFour)
{
    uint8_t given[] = "\x10\xBF\xFE\x82\x84";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 4, 0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_MALFORMED_PACKET, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
}

TEST(mqtt_frame_deserialize, CheckIfPayloadReceivedCorrectly)
{
    uint8_t given[] = "\x10\x0CMQTT_PAYLOAD";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    expect_payload(&given[2], 12, 0);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
    TEST_ASSERT_EQUAL_STRING(&given[2], g_desc->payload);
}

TEST(mqtt_frame_deserialize, FailBecauseCantReceiveHeader)
{
    mqtt_io_read_ExpectAndReturn(NULL, NULL, 1, 0, MQTT_ERRNO_BROKEN_PIPE);
    mqtt_io_read_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
}

TEST(mqtt_frame_deserialize, FailBecauseCantReceiveRemainingLength)
{
    uint8_t given[] = "\x10";
    expect_hdr(&given[0], 0);
    mqtt_io_read_ExpectAndReturn(NULL, NULL, 1, 0, MQTT_ERRNO_BROKEN_PIPE);
    mqtt_io_read_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
}

TEST(mqtt_frame_deserialize, FailBecauseCantReceivePayload)
{
    uint8_t given[] = "\x10\x02MQ";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0);
    mqtt_io_read_ExpectAndReturn(NULL, NULL, 2, 0, MQTT_ERRNO_BROKEN_PIPE);
    mqtt_io_read_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 0));
}

TEST(mqtt_frame_deserialize, FailBecauseIncorrectDescriptorSize)
{
    TEST_ASSERT_EQUAL(MQTT_ERRNO_INVALID_VALUE, mqtt_frame_deserialize(NULL, g_desc, sizeof(mqtt_frame_t) - 1, 0));
}
TEST(mqtt_frame_deserialize, FailBecauseInputBufferForPayloadTooSmall)
{
    uint8_t given[] = "\x10\x0CMQTT_PAYLOAD";
    expect_hdr(&given[0], 0);
    expect_remlen(&given[1], 1, 0); 
    // max payload lenth = (size - sizeof(mqtt_frame_t)); 19 - 8 = 11.
    // cant fit 12 byte payload in 11 byte buffer
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BUFFER_OVF, mqtt_frame_deserialize(NULL, g_desc, 19,0));
}

/* ========================================================================================= */

TEST_GROUP(mqtt_frame_deserialize_timeout);

TEST_SETUP(mqtt_frame_deserialize_timeout)
{
    system_mock_Init();
    g_desc_len = 1024;
    g_desc = calloc(1, g_desc_len);
}

TEST_TEAR_DOWN(mqtt_frame_deserialize_timeout)
{
    free(g_desc);
}

TEST(mqtt_frame_deserialize_timeout, TimeoutBecauseDeadlineReached)
{
    mqtt_clock_ExpectAndReturn(10);
    TEST_ASSERT_EQUAL(MQTT_ERRNO_TIMEOUT, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 10));
}

TEST(mqtt_frame_deserialize_timeout, BrokenPipeCausedByTimeout)
{
    mqtt_clock_ExpectAndReturn(1);
    expect_hdr(&g_expected_frame[0], (10 - 1));
    mqtt_clock_ExpectAndReturn(10);
    expect_remlen(&g_expected_frame[1], 1, (10 - 10));
    TEST_ASSERT_EQUAL(MQTT_ERRNO_BROKEN_PIPE, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 10));
}

TEST(mqtt_frame_deserialize_timeout, InTime)
{
    mqtt_clock_ExpectAndReturn(1);
    expect_hdr(&g_expected_frame[0], (10 - 1));
    mqtt_clock_ExpectAndReturn(2);
    expect_remlen(&g_expected_frame[1], 1, (10 - 2));
    mqtt_clock_ExpectAndReturn(3);
    expect_payload(&g_expected_frame[2], 12, (10 - 3));
    TEST_ASSERT_EQUAL(MQTT_ERRNO_OK, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 10));
}

TEST(mqtt_frame_deserialize_timeout, NetworkTimeout)
{
    mqtt_clock_IgnoreAndReturn(8);
    mqtt_io_read_ExpectAndReturn(NULL, NULL, 1, (10 - 8), MQTT_ERRNO_TIMEOUT);
    mqtt_io_read_IgnoreArg_buffer();
    TEST_ASSERT_EQUAL(MQTT_ERRNO_TIMEOUT, mqtt_frame_deserialize(NULL, g_desc, g_desc_len, 10));
}


