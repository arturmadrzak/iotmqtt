#include "unity_fixture.h"
    
TEST_GROUP_RUNNER(out_packet_connect)
{
    RUN_TEST_CASE(out_packet_connect, HeaderPacketType);
    RUN_TEST_CASE(out_packet_connect, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_connect, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_connect, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_connect, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_connect, ProtocolNameLength);
    RUN_TEST_CASE(out_packet_connect, ProtocolName);
    RUN_TEST_CASE(out_packet_connect, ProtocolLevel);
    RUN_TEST_CASE(out_packet_connect, KeepAliveOnTwoBytes)
    RUN_TEST_CASE(out_packet_connect, ClientId);
    RUN_TEST_CASE(out_packet_connect, ClientIdTooLong);
}

TEST_GROUP_RUNNER(out_packet_connect_flags)
{
    RUN_TEST_CASE(out_packet_connect_flags, ReservedFlag);
    RUN_TEST_CASE(out_packet_connect_flags, UserNameFlag);
    RUN_TEST_CASE(out_packet_connect_flags, PasswordFlagWhenNoUserFlag);
    RUN_TEST_CASE(out_packet_connect_flags, PasswordFlag);
    RUN_TEST_CASE(out_packet_connect_flags, WillFlag);
    RUN_TEST_CASE(out_packet_connect_flags, WillQoS1);
    RUN_TEST_CASE(out_packet_connect_flags, WillQoS2);
    RUN_TEST_CASE(out_packet_connect_flags, InvalidWillQoS);
    RUN_TEST_CASE(out_packet_connect_flags, WillRetain);
    RUN_TEST_CASE(out_packet_connect_flags, CleanSession);
}

TEST_GROUP_RUNNER(out_packet_connect_optional_payload)
{
    RUN_TEST_CASE(out_packet_connect_optional_payload, WillTopicField);
    RUN_TEST_CASE(out_packet_connect_optional_payload, WillMessageField);
    RUN_TEST_CASE(out_packet_connect_optional_payload, UserNameField);
    RUN_TEST_CASE(out_packet_connect_optional_payload, PasswordField);
    RUN_TEST_CASE(out_packet_connect_optional_payload, PasswordFieldExistsButNoUserName);
}

TEST_GROUP_RUNNER(out_packet_publish)
{
    RUN_TEST_CASE(out_packet_publish, HeaderPacketType);
    RUN_TEST_CASE(out_packet_publish, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_publish, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_publish, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_publish, HeaderPacketRetain);

    RUN_TEST_CASE(out_packet_publish, CheckQoS2);
    RUN_TEST_CASE(out_packet_publish, CheckWrongQoS);
    RUN_TEST_CASE(out_packet_publish, CheckRetain);
    RUN_TEST_CASE(out_packet_publish, CheckTopicName);
    RUN_TEST_CASE(out_packet_publish, CheckPacketIdIsAbsentWithQoS0);
    RUN_TEST_CASE(out_packet_publish, CheckPacketIdWithQoS2);
    RUN_TEST_CASE(out_packet_publish, CheckPayloadWithQoS0);
    RUN_TEST_CASE(out_packet_publish, CheckPayloadWithQoS2);
    RUN_TEST_CASE(out_packet_publish, ZeroLengthMessage);
}

TEST_GROUP_RUNNER(out_packet_puback)
{
    RUN_TEST_CASE(out_packet_puback, HeaderPacketType);
    RUN_TEST_CASE(out_packet_puback, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_puback, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_puback, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_puback, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_puback, PacketIdOnPayloadBegin);
}

TEST_GROUP_RUNNER(out_packet_pubrec)
{
    RUN_TEST_CASE(out_packet_pubrec, HeaderPacketType);
    RUN_TEST_CASE(out_packet_pubrec, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_pubrec, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_pubrec, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_pubrec, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_pubrec, PacketIdOnPayloadBegin);
}

TEST_GROUP_RUNNER(out_packet_pubrel)
{
    RUN_TEST_CASE(out_packet_pubrel, HeaderPacketType);
    RUN_TEST_CASE(out_packet_pubrel, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_pubrel, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_pubrel, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_pubrel, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_pubrel, PacketIdOnPayloadBegin);
}

TEST_GROUP_RUNNER(out_packet_pubcomp)
{
    RUN_TEST_CASE(out_packet_pubcomp, HeaderPacketType);
    RUN_TEST_CASE(out_packet_pubcomp, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_pubcomp, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_pubcomp, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_pubcomp, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_pubcomp, PacketIdOnPayloadBegin);
}

TEST_GROUP_RUNNER(out_packet_subscribe)
{
    RUN_TEST_CASE(out_packet_subscribe, HeaderPacketType);
    RUN_TEST_CASE(out_packet_subscribe, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_subscribe, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_subscribe, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_subscribe, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_subscribe, PacketId);
    RUN_TEST_CASE(out_packet_subscribe, EmptyList);
    RUN_TEST_CASE(out_packet_subscribe, OneTopicLength);
    RUN_TEST_CASE(out_packet_subscribe, OneTopicValue);
    RUN_TEST_CASE(out_packet_subscribe, OneTopicQoS);
    RUN_TEST_CASE(out_packet_subscribe, TopicListValues);
    RUN_TEST_CASE(out_packet_subscribe, TopicListQoS);
    RUN_TEST_CASE(out_packet_subscribe, NullTopicName);
    RUN_TEST_CASE(out_packet_subscribe, InvalidQoS);
}

TEST_GROUP_RUNNER(out_packet_unsubscribe)
{
    RUN_TEST_CASE(out_packet_unsubscribe, HeaderPacketType);
    RUN_TEST_CASE(out_packet_unsubscribe, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_unsubscribe, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_unsubscribe, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_unsubscribe, HeaderPacketRetain);
    RUN_TEST_CASE(out_packet_unsubscribe, PacketId);
    RUN_TEST_CASE(out_packet_unsubscribe, EmptyList);
    RUN_TEST_CASE(out_packet_unsubscribe, OneTopicLength);
    RUN_TEST_CASE(out_packet_unsubscribe, OneTopicValue);
    RUN_TEST_CASE(out_packet_unsubscribe, TopicListValues);
}

TEST_GROUP_RUNNER(out_packet_pingreq)
{
    RUN_TEST_CASE(out_packet_pingreq, HeaderPacketType);
    RUN_TEST_CASE(out_packet_pingreq, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_pingreq, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_pingreq, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_pingreq, HeaderPacketRetain);
}

TEST_GROUP_RUNNER(out_packet_disconnect)
{
    RUN_TEST_CASE(out_packet_disconnect, HeaderPacketType);
    RUN_TEST_CASE(out_packet_disconnect, HeaderPacketLength);
    RUN_TEST_CASE(out_packet_disconnect, HeaderPacketQoS);
    RUN_TEST_CASE(out_packet_disconnect, HeaderPacketDup);
    RUN_TEST_CASE(out_packet_disconnect, HeaderPacketRetain);
}

TEST_GROUP_RUNNER(in_packet_connack)
{
    RUN_TEST_CASE(in_packet_connack, InvalidType);
    RUN_TEST_CASE(in_packet_connack, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_connack, InvalidReservedValues);
    RUN_TEST_CASE(in_packet_connack, SessionPresentAndConnected);
    RUN_TEST_CASE(in_packet_connack, SessionDoesNotPresent);
    RUN_TEST_CASE(in_packet_connack, ReturnCodeOk);
    RUN_TEST_CASE(in_packet_connack, ReturnCodeFailure);
}

TEST_GROUP_RUNNER(in_packet_publish)
{
    RUN_TEST_CASE(in_packet_publish, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_publish, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_publish, PacketId);
    RUN_TEST_CASE(in_packet_publish, MsgQoS0);
    RUN_TEST_CASE(in_packet_publish, MsgQoS2);
    RUN_TEST_CASE(in_packet_publish, MsgRetain);
    RUN_TEST_CASE(in_packet_publish, Message);
}
TEST_GROUP_RUNNER(in_packet_puback)
{
    RUN_TEST_CASE(in_packet_puback, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_puback, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_puback, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_puback, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_puback, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_puback, PacketId);
}

TEST_GROUP_RUNNER(in_packet_pubrec)
{
    RUN_TEST_CASE(in_packet_pubrec, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_pubrec, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_pubrec, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_pubrec, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_pubrec, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_pubrec, PacketId);
}

TEST_GROUP_RUNNER(in_packet_pubrel)
{
    RUN_TEST_CASE(in_packet_pubrel, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_pubrel, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_pubrel, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_pubrel, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_pubrel, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_pubrel, PacketId);
}

TEST_GROUP_RUNNER(in_packet_pubcomp)
{
    RUN_TEST_CASE(in_packet_pubcomp, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_pubcomp, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_pubcomp, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_pubcomp, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_pubcomp, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_pubcomp, PacketId);
}

TEST_GROUP_RUNNER(in_packet_unsuback)
{
    RUN_TEST_CASE(in_packet_unsuback, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_unsuback, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_unsuback, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_unsuback, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_unsuback, InvalidPayloadLength);
    RUN_TEST_CASE(in_packet_unsuback, PacketId);
}

TEST_GROUP_RUNNER(in_packet_pingresp)
{
    RUN_TEST_CASE(in_packet_pingresp, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_pingresp, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_pingresp, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_pingresp, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_pingresp, InvalidPayloadLength);
}

TEST_GROUP_RUNNER(in_packet_suback)
{
    RUN_TEST_CASE(in_packet_suback, InvalidHeaderPacketType);
    RUN_TEST_CASE(in_packet_suback, InvalidHeaderDup);
    RUN_TEST_CASE(in_packet_suback, InvalidHeaderQoS);
    RUN_TEST_CASE(in_packet_suback, InvalidHeaderRetain);
    RUN_TEST_CASE(in_packet_suback, PacketId);
    RUN_TEST_CASE(in_packet_suback, IncomeMoreAckThanCanHandle);
    RUN_TEST_CASE(in_packet_suback, SubacksNumber);
    RUN_TEST_CASE(in_packet_suback, ReturnCodesQoS);
    RUN_TEST_CASE(in_packet_suback, InvalidReturnCode);
}

static void RunAllTests(void)
{
    RUN_TEST_GROUP(out_packet_connect);
    RUN_TEST_GROUP(out_packet_connect_flags);
    RUN_TEST_GROUP(out_packet_connect_optional_payload);
    RUN_TEST_GROUP(out_packet_publish);
    RUN_TEST_GROUP(out_packet_puback);
    RUN_TEST_GROUP(out_packet_pubrec);
    RUN_TEST_GROUP(out_packet_pubrel);
    RUN_TEST_GROUP(out_packet_pubcomp);
    RUN_TEST_GROUP(out_packet_subscribe)
    RUN_TEST_GROUP(out_packet_unsubscribe)
    RUN_TEST_GROUP(out_packet_pingreq);
    RUN_TEST_GROUP(out_packet_disconnect);
    RUN_TEST_GROUP(in_packet_publish);
    RUN_TEST_GROUP(in_packet_connack);
    RUN_TEST_GROUP(in_packet_puback);
    RUN_TEST_GROUP(in_packet_pubrec);
    RUN_TEST_GROUP(in_packet_pubrel);
    RUN_TEST_GROUP(in_packet_pubcomp);
    RUN_TEST_GROUP(in_packet_unsuback);
    RUN_TEST_GROUP(in_packet_pingresp);
    RUN_TEST_GROUP(in_packet_suback);
}

int main(int argc, const char *argv[])
{
    return UnityMain(argc, argv, RunAllTests);
}
