#include "unity_fixture.h"

TEST_GROUP_RUNNER(mqtt_frame_serialize)
{
    RUN_TEST_CASE(mqtt_frame_serialize, FailBecauseInvalidControlPacketType);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfControlPacketTypeSetCorrectly);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfDuplicateFlagIsSet);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfQoSFlagsAreNotSetOnQoS0);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfQoSFlagsAreSetOnQoS1);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfQoSFlagsAreSetOnQoS2);
    RUN_TEST_CASE(mqtt_frame_serialize, FailBecauseNotAllowedQoS);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfRetainFlagIsSet);
    RUN_TEST_CASE(mqtt_frame_serialize, RemainingLengthEncodedOnOneByte);
    RUN_TEST_CASE(mqtt_frame_serialize, RemainingLengthEncodedOnTwoBytes);
    RUN_TEST_CASE(mqtt_frame_serialize, RemainingLengthEncodedOnTwoThree);
    RUN_TEST_CASE(mqtt_frame_serialize, RemainingLengthEncodedOnTwoFour);
    RUN_TEST_CASE(mqtt_frame_serialize, InvalidRemainingLengthEncodedOnTwoFour);
    RUN_TEST_CASE(mqtt_frame_serialize, CheckIfPayloadSendCorrectly);
    RUN_TEST_CASE(mqtt_frame_serialize, FailBecauseCantSendHeader);
    RUN_TEST_CASE(mqtt_frame_serialize, FailBecauseCantSendRemainingLength);
    RUN_TEST_CASE(mqtt_frame_serialize, FailBecauseCantSendPayload);
}

TEST_GROUP_RUNNER(mqtt_frame_serialize_timeout)
{
    RUN_TEST_CASE(mqtt_frame_serialize_timeout, TimeoutBecauseDeadlineReached);
    RUN_TEST_CASE(mqtt_frame_serialize_timeout, BrokenPipeCausedByTimeout);
    RUN_TEST_CASE(mqtt_frame_serialize_timeout, InTime);
    RUN_TEST_CASE(mqtt_frame_serialize_timeout, NetworkTimeout);
}

TEST_GROUP_RUNNER(mqtt_frame_deserialize)
{
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseInvalidControlPacketType);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfControlPacketTypeSetCorrectly);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfDuplicateFlagIsSet);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfQoSFlagsAreNotSetOnQoS0);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfQoSFlagsAreSetOnQoS1);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfQoSFlagsAreSetOnQoS2);
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseNotAllowedQoS);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfRetainFlagIsSet);    
    RUN_TEST_CASE(mqtt_frame_deserialize, RemainingLengthEncodedOnOneByte);
    RUN_TEST_CASE(mqtt_frame_deserialize, RemainingLengthEncodedOnTwoBytes);
    RUN_TEST_CASE(mqtt_frame_deserialize, RemainingLengthEncodedOnTwoThree);
    RUN_TEST_CASE(mqtt_frame_deserialize, RemainingLengthEncodedOnTwoFour);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckRemainingLengthUpdatedIfFrameDescNotZero);
    RUN_TEST_CASE(mqtt_frame_deserialize, InvalidRemainingLengthEncodedOnTwoFour);
    RUN_TEST_CASE(mqtt_frame_deserialize, CheckIfPayloadReceivedCorrectly);
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseCantReceiveHeader);
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseCantReceiveRemainingLength);
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseCantReceivePayload);
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseIncorrectDescriptorSize);
    RUN_TEST_CASE(mqtt_frame_deserialize, FailBecauseInputBufferForPayloadTooSmall);
}

TEST_GROUP_RUNNER(mqtt_frame_deserialize_timeout)
{
    RUN_TEST_CASE(mqtt_frame_deserialize_timeout, TimeoutBecauseDeadlineReached);
    RUN_TEST_CASE(mqtt_frame_deserialize_timeout, BrokenPipeCausedByTimeout);
    RUN_TEST_CASE(mqtt_frame_deserialize_timeout, InTime);
    RUN_TEST_CASE(mqtt_frame_deserialize_timeout, NetworkTimeout);
}

static void RunAllTests(void)
{
    RUN_TEST_GROUP(mqtt_frame_serialize);
    RUN_TEST_GROUP(mqtt_frame_serialize_timeout);
    RUN_TEST_GROUP(mqtt_frame_deserialize);
    RUN_TEST_GROUP(mqtt_frame_deserialize_timeout);
}

int main(int argc, const char *argv[])
{
    return UnityMain(argc, argv, RunAllTests);
}

