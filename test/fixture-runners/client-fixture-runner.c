#include "unity_fixture.h"


TEST_GROUP_RUNNER(mqtt_connect)
{
    RUN_TEST_CASE(mqtt_connect, InvalidConnectOptions);
    RUN_TEST_CASE(mqtt_connect, SendingFrameFailed);
    RUN_TEST_CASE(mqtt_connect, ReceiveFrameError);
    RUN_TEST_CASE(mqtt_connect, ReceivedInvalidConnackOrUnexpectedMessage);
    RUN_TEST_CASE(mqtt_connect, CheckSessionPresentReceivedCorrectly);
    RUN_TEST_CASE(mqtt_connect, IgnoreSessionPresentReturnIfNullPointerProvided);
    RUN_TEST_CASE(mqtt_connect, ConnectedSuccessfully);
    RUN_TEST_CASE(mqtt_connect, BrokerRefusedConnection);
    RUN_TEST_CASE(mqtt_connect, CheckIfSessionDataInitializedCorrectly);
    RUN_TEST_CASE(mqtt_connect, CheckCorrectDeadlinePassedToFunctions);
}

TEST_GROUP_RUNNER(mqtt_disconnect)
{
    RUN_TEST_CASE(mqtt_disconnect, SendingFrameFailed);
    RUN_TEST_CASE(mqtt_disconnect, PassDeadlineValueAndExpectTimeout);
}

TEST_GROUP_RUNNER(mqtt_publish_qos0)
{
    RUN_TEST_CASE(mqtt_publish_qos0, InvalidPublishData);
    RUN_TEST_CASE(mqtt_publish_qos0, SendingPublishFrameFailed);
    RUN_TEST_CASE(mqtt_publish_qos0, CheckIfPacketIdIncremented);
    RUN_TEST_CASE(mqtt_publish_qos0, CheckIfLastSentTimeUpdated);
}

TEST_GROUP_RUNNER(mqtt_publish)
{
    RUN_TEST_CASE(mqtt_publish, ReceiveError);
    RUN_TEST_CASE(mqtt_publish, ReceivedMalformedPacket);
    RUN_TEST_CASE(mqtt_publish, HandlePublishRequestDuringSession);
    RUN_TEST_CASE(mqtt_publish, HandlePingrespDuringSession);
    RUN_TEST_CASE(mqtt_publish, FailBecauseReceivedUnexpectedPacket);
    RUN_TEST_CASE(mqtt_publish, FailBecauseReceivedInvalidPacketId);
    RUN_TEST_CASE(mqtt_publish, PubackReceivedSuccessfully);
    RUN_TEST_CASE(mqtt_publish, PubcompReceivedSuccessfully);
    RUN_TEST_CASE(mqtt_publish, CheckPubrelSentAfterPubrec);
    RUN_TEST_CASE(mqtt_publish, CheckPacketIdIncrementedIfError);
    RUN_TEST_CASE(mqtt_publish, CheckPacketIdIncrementedIfSuccess);
    RUN_TEST_CASE(mqtt_publish, CheckIfLastSentTimeUpdatedIfSuccess);
    RUN_TEST_CASE(mqtt_publish, CheckIfLastSentTimeNotUpdatedIfError);
    RUN_TEST_CASE(mqtt_publish, CheciIfPendingOPublishCleared);
}

TEST_GROUP_RUNNER(mqtt_subscribe)
{
    RUN_TEST_CASE(mqtt_subscribe, InvalidSubscribeOpts);
    RUN_TEST_CASE(mqtt_subscribe, FailBecauseBufferTooSmallToFitTopics);
    RUN_TEST_CASE(mqtt_subscribe, SendingSubscribeFrameFailed);
    RUN_TEST_CASE(mqtt_subscribe, ReceiveError);
    RUN_TEST_CASE(mqtt_subscribe, ReceivedMalformedPacket);
    RUN_TEST_CASE(mqtt_subscribe, HandlePublishRequestDuringSession);
    RUN_TEST_CASE(mqtt_subscribe, HandlePingrespDuringSession);
    RUN_TEST_CASE(mqtt_subscribe, FailBecauseReceivedUnexpectedPacket);
    RUN_TEST_CASE(mqtt_subscribe, FailBecauseReceivedInvalidPacketId);
    RUN_TEST_CASE(mqtt_subscribe, SubackLessReturnCodesCountThanExpected);
    RUN_TEST_CASE(mqtt_subscribe, SubackMoreReturnCodesCountThanExpected);
    RUN_TEST_CASE(mqtt_subscribe, SubackSingleFailure);
    RUN_TEST_CASE(mqtt_subscribe, SubackSubWithWrongQoS);
    RUN_TEST_CASE(mqtt_subscribe, LastSentTimeUpdated);
}

TEST_GROUP_RUNNER(remote_host_publish)
{
    //RUN_TEST_CASE(remote_host_publish, mqtt_subscribe);
}

TEST_GROUP_RUNNER(keep_alive)
{
    //RUN_TEST_CASE(keep_alive, mqtt_publish);
}

TEST_GROUP_RUNNER(check_deadlines)
{
    RUN_TEST_CASE(check_deadlines, mqtt_publish);
    RUN_TEST_CASE(check_deadlines, mqtt_subscribe);
}

static void RunAllTests(void)
{
    RUN_TEST_GROUP(mqtt_connect);
    RUN_TEST_GROUP(mqtt_disconnect);
    RUN_TEST_GROUP(mqtt_publish_qos0);
    RUN_TEST_GROUP(mqtt_publish);
    RUN_TEST_GROUP(mqtt_subscribe);
    RUN_TEST_GROUP(remote_host_publish)
    RUN_TEST_GROUP(keep_alive)
    RUN_TEST_GROUP(check_deadlines)
}

int main(int argc, const char *argv[])
{
    return UnityMain(argc, argv, RunAllTests);
}

