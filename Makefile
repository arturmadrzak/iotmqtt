BUILDDIR ?= build
PORT ?= linux

SRCS = $(wildcard src/*.c)
OBJS = $(addprefix $(BUILDDIR)/,$(SRCS:.c=.o))

CFLAGS = -Iinclude \
		 -o0 -ggdb3

.PHONY: all lib sample clean

all: lib sample

lib: $(BUILDDIR)/libiotmqtt.a

sample: lib
	make -C sample BUILDDIR=../$(BUILDDIR) 

$(BUILDDIR)/libiotmqtt.a: $(OBJS)
	$(AR) csr $@ $^

$(BUILDDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
