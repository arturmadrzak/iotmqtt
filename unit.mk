NAME:=iotmqtt

BUILDDIR?=build/test
V=@


CMOCK_DIR?=3rdparty/CMock
UNITY_DIR?=3rdparty/Unity

# Environment
RUBY ?= ruby
GEN_RUNNER := $(RUBY) $(UNITY_DIR)/auto/generate_test_runner.rb
GEN_MOCK = CMOCK_DIR=./$(CMOCK_DIR) \
	MOCK_PREFIX='' \
	MOCK_SUFFIX='-mock' \
	MOCK_INCLUDE_C_PRE_HEADER='mqtt.h' \
	$(RUBY) $(CMOCK_DIR)/scripts/create_mock.rb

CC=gcc

MOCK_HEDERS := include/packet.h include/frame.h include/system.h
MOCKS_LOCAL := $(patsubst include/%.h,mock/%-mock.c,$(MOCK_HEDERS))
MOCKS := $(MOCKS_LOCAL)
LIBMOCKS := $(BUILDDIR)/libmocks.a

TESTS := $(wildcard test/*-test.c)
FIXTURES := $(wildcard test/*-fixture.c)
RUNNERS := $(patsubst test/%-test.c,runner/%-runner.c,$(TESTS))
FIXTURE_RUNNERS := $(wildcard test/fixture-runners/*-fixture-runner.c)
EXEC := $(patsubst test/%-test.c,$(BUILDDIR)/%-test,$(TESTS))
FIXTURE_EXEC:= $(patsubst test/%-fixture.c,$(BUILDDIR)/%-fixture,$(FIXTURES)) 

INCLUDES := include \
			mock \
			port/linux \
			$(UNITY_DIR)/src \
			$(UNITY_DIR)/extras/fixture/src \
			$(CMOCK_DIR)/src 


# Dependency files
# TODO: it doesn't look nice
DEPS_OBJS=
DEPS=$(DEPS_OBJS:.o=.d)

CFLAGS = -std=c99 -m32 -static -O0 -ggdb3 -Wall -Wextra -Wunused-macros -DUNIT_TEST=1
LDFLAGS = -m32 \
		  -L$(BUILDDIR) \
		  -L$(BUILDDIR)/3rdparty \
		  -lmocks -lunity -lcmock  
# Declare PHONY targets
.PHONY: all run libs clean

# Define phony targets
all: $(LIBMOCKS) libs $(EXEC) $(FIXTURE_EXEC)

run: $(EXEC) $(FIXTURE_EXEC)
	@echo "Executing tests"
	$(foreach i,$^,./$(i) -v &&) true

libs:
	@make -C 3rdparty -f unit.mk all CFLAGS="$(CFLAGS)" BUILDDIR="../$(BUILDDIR)/3rdparty"

$(LIBMOCKS): $(addprefix $(BUILDDIR)/,$(MOCKS:.c=.o))
	@mkdir -p $(dir $@)
	$(AR) csr $@ $^

$(EXEC): $(BUILDDIR)/%-test: $(BUILDDIR)/src/%.o $(BUILDDIR)/test/%-test.o $(BUILDDIR)/runner/%-runner.o 
	@mkdir -p $(dir $@)
	$(CC) -o $@ $^ $(LDFLAGS) 

$(FIXTURE_EXEC): $(BUILDDIR)/%-fixture: $(BUILDDIR)/src/%.o $(BUILDDIR)/test/%-fixture.o $(BUILDDIR)/test/fixture-runners/%-fixture-runner.o
	@mkdir -p $(dir $@)
	$(CC) -o $@ $^ $(LDFLAGS) 

$(BUILDDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) -c $< -o $@ $(CFLAGS) $(addprefix -I,$(INCLUDES)) 

$(RUNNERS): runner/%-runner.c: test/%-test.c
	@mkdir -p $(dir $@)
	$(GEN_RUNNER) $< $@ > /dev/null

$(MOCKS_LOCAL): mock/%-mock.c: include/%.h
	@mkdir -p $(dir $@)
	MOCK_OUT=$(dir $@) $(GEN_MOCK) $<

clean:
		@echo "Clean"
		@rm -fr $(BUILDDIR)
		@rm -fr runners
		@rm -rf mock

# Dependencies
-include $(DEPS)

