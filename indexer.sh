#!/usr/bin/env bash

DIRS="3rdparty/Unity
      3rdparty/CMock
      include
      src
      mock 
      test"

function index_root()
{
    find -L $1 -name *.[ch] >> cscope.files
}

rm -f cscope.files
for p in $DIRS; do
    echo "Preparing $p"
    index_root $p
done

cscope -Rbk
ctags -R

