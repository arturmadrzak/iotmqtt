BUILDDIR?=build
V?=@

LIBUNITY:=$(BUILDDIR)/libunity.a
LIBCMOCK:=$(BUILDDIR)/libcmock.a
INCLUDES:=Unity/src Unity/extras/fixture/src CMock/src

all: $(LIBUNITY) $(LIBCMOCK) $(LIBFRMOCK)

$(LIBUNITY): $(BUILDDIR)/Unity/src/unity.o $(BUILDDIR)/Unity/extras/fixture/src/unity_fixture.o
	@mkdir -p $(dir $@)
	$(AR) csr $@ $^
	
$(LIBCMOCK): $(BUILDDIR)/CMock/src/cmock.o
	@mkdir -p $(dir $@)
	$(AR) csr $@ $^

$(BUILDDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(addprefix -I,$(INCLUDES)) $< -o $@

	

