" invoke indexer script
nmap <F2> :silent exec "!./indexer.sh"<CR>:cs reset<CR>:redraw!<CR>:echom 'Index updated'<CR>
" build project
nmap <F5> :make<CR>:copen<CR>:cw<CR>
" clean project
nmap <F6> :make clean<CR>
" build unit tests
nmap <F7> :make -f unit.mk<CR>:copen<CR>:cw<CR>
" run unit tests
nmap <F8> :make -f unit.mk run<CR>

set undodir=.undo
set undofile

